package com.uscopex.models;

public class DiscoverPipelinesModel {

    private int iD;
    private String name;
    private int versions;
    private String description;
    private int batchable;
    private String author;

    public DiscoverPipelinesModel() { }

    public int getiD() {
        return iD;
    }

    public String getName() {
        return name;
    }

    public int getVersions() {
        return versions;
    }

    public String getDescription() {
        return description;
    }

    public int isBatchable() {
        return batchable;
    }

    public String getAuthor() {
        return author;
    }
}
