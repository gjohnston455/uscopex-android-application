package com.uscopex.models;

import com.google.gson.annotations.SerializedName;

public class Pipeline {

    @SerializedName("ID")
    private int ID;

    @SerializedName("Name")
    private String Name;

    @SerializedName("Description")
    private String Description;

    @SerializedName("Pipeline_Path")
    private String Pipeline_Path;

    @SerializedName("Versions")
    private int Versions;

    @SerializedName("Date_Added")
    private String Date_Added;

    @SerializedName("author_name")
    private String author_name;

    private boolean roi;
    private boolean scale;

    public Pipeline() {
    }


    public Pipeline(int ID, String Name, String Description, String Pipeline_Path, int Versions, String Date_Added, String author_name) {
        this.ID = ID;
        this.Name = Name;
        this.Description = Description;
        this.Pipeline_Path = Pipeline_Path;
        this.Versions = Versions;
        this.Date_Added = Date_Added;
        this.author_name = author_name;
    }

    public int getID() { return ID; }

    public void setID(int ID) { this.ID = ID; }

    public String getName() {
        return Name;
    }

    public String getDescription() {
        return Description;
    }

    public String getPipeline_Path() {
        return Pipeline_Path;
    }

    public int getVersions() {
        return Versions;
    }

    public String getDate_Added() {
        return Date_Added;
    }

    public String getAuthor_name() {
        return author_name;
    }

    public void setName(String name) {
        Name = name;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public void setPipeline_Path(String pipeline_Path) {
        Pipeline_Path = pipeline_Path;
    }

    public void setVersions(int versions) {
        Versions = versions;
    }

    public void setDate_Added(String date_Added) {
        Date_Added = date_Added;
    }

    public void setAuthor_name(String author_name) {
        this.author_name = author_name;
    }

    public boolean isRoi() {
        return roi;
    }

    public void setRoi(boolean roi) {
        this.roi = roi;
    }

    public boolean isScale() {
        return scale;
    }

    public void setScale(boolean scale) {
        this.scale = scale;
    }
}
