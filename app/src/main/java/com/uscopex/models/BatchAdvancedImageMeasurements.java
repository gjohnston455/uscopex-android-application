package com.uscopex.models;

public class BatchAdvancedImageMeasurements {

    private ROI roi;
    private Scale scale;

    public BatchAdvancedImageMeasurements(){}

    public ROI getRoi() {
        return roi;
    }

    public Scale getScale() {
        return scale;
    }

    public void setRoi(ROI roi) {
        this.roi = roi;
    }

    public void setScale(Scale scale) {
        this.scale = scale;
    }
}
