package com.uscopex.models;

public class ICMResultSet {

    private int iD;
    private  String name;
    private int classification;
    private String date;
    private String description;
    private String modelName;
    private String classNames;

    public ICMResultSet(int iD, String name, int classification, String date, String description, String modelName, String classNames) {
        this.iD = iD;
        this.name = name;
        this.classification = classification;
        this.date = date;
        this.description = description;
        this.modelName = modelName;
        this.classNames = classNames;
    }

    public int getiD() {
        return iD;
    }

    public String getName() {
        return name;
    }

    public int getClassification() {
        return classification;
    }

    public String getDate() {
        return date;
    }

    public String getDescription() {
        return description;
    }

    public String getModelName() {
        return modelName;
    }

    public String getClassNames() {
        return classNames;
    }
}
