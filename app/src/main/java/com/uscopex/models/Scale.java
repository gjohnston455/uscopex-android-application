package com.uscopex.models;

/*
Custom scale model class for housing scale measurement data
Model is mapped to IAM Api
 */
public class Scale {

    private int knownDistance;
    private String unitOfMeasurement;
    private int calculatedDistance;

    public Scale() { }

    public Scale(int knownDistance, String unitOfMeasurement, int calculatedDistance) {
        super();
        this.calculatedDistance = calculatedDistance;
        this.knownDistance = knownDistance;
        this.unitOfMeasurement = unitOfMeasurement;
    }
}
