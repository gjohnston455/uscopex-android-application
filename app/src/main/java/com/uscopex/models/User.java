package com.uscopex.models;

public class User {

    private int accountId;
    private String firstName;
    private String lastName;
    private String email;
    private int loginId;


    public User( int accountId, String firstName, String lastName, String email, int loginId ) {
        this.accountId = accountId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.loginId = loginId;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public int getLoginId() {
        return loginId;
    }

    public int getAccountId() {
        return accountId;
    }
}
