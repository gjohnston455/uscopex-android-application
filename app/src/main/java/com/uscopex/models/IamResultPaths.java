package com.uscopex.models;

public class IamResultPaths {

    private String resultPath;
    private String imagePath;

    public IamResultPaths() { }

    public IamResultPaths(String resultPath, String imagePath) {
        this.resultPath = resultPath;
        this.imagePath = imagePath;
    }

    public String getResultPath() {
        return resultPath;
    }

    public String getImagePath() {
        return imagePath;
    }
}
