package com.uscopex.models;

public class IAMResultSet {

    private int iD;
    private  String name;
    private String date;
    private String pipeline;
    private String pipelineVersion;
    private String description;
    private String imagePath;
    private String resultPath;


    public IAMResultSet(int iD, String name, String date, String pipeline, String pipelineVersion, String description, String imagePath, String resultPath) {
        this.iD = iD;
        this.name = name;
        this.date = date;
        this.pipeline = pipeline;
        this.pipelineVersion = pipelineVersion;
        this.description = description;
        this.imagePath = imagePath;
        this.resultPath = resultPath;

    }
    public String getImagePath() {
        return imagePath;
    }

    public String getResultPath() {
        return resultPath;
    }

    public int getiD() {
        return iD;
    }

    public String getName() {
        return name;
    }

    public String getDate() {
        return date;
    }

    public String getPipeline() {
        return pipeline;
    }

    public String getPipelineVersion() {
        return pipelineVersion;
    }

    public String getDescription() {
        return description;
    }

}
