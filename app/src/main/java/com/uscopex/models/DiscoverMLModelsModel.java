package com.uscopex.models;

public class DiscoverMLModelsModel {

    private int iD;
    private String name;
    private String description;
    private String author;
    private String date;
    private String classNames;

    public DiscoverMLModelsModel() { }

    public int getiD() {
        return iD;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getAuthor() {
        return author;
    }

    public String getDate() {
        return date;
    }

    public String getClassNames() {
        return classNames;
    }


}
