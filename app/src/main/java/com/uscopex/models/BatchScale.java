package com.uscopex.models;

/*
Required to be able to send an array of scale's in the com.uscopex.api call for advanced batch processing
 */
public class BatchScale {

    private Scale[] scaleArray;

    public BatchScale(Scale[] scaleArray) {
        this.scaleArray = scaleArray;
    }
}
