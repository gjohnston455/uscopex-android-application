package com.uscopex.models;

/*
Required to be able to send an array of ROI's in the com.uscopex.api call for advanced batch processing
 */
public class BatchROI {

    private ROI[] roiArray;

    public BatchROI(ROI[] roiArray) {
        this.roiArray = roiArray;
    }
}
