package com.uscopex.models;

/*
Custom ROI model class for housing ROI measurement data
Model is mapped to IAM Api
 */
public class ROI {

    private int x;
    private int y;
    private int width;
    private int height;

    public ROI(int x, int y, int width, int height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    @Override
    public String toString() {
        return "ROI{" +
                "x=" + x +
                ", y=" + y +
                ", width=" + width +
                ", height=" + height +
                '}';
    }
}
