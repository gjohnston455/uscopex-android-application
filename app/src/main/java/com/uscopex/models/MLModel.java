package com.uscopex.models;

public class MLModel {
    private int ID;
    private String Name;
    private String Description;
    private String Date_Added;
    private String author_name;
    private String ClassNames;

    public MLModel(){ }


    public MLModel(int id, String Name, String Description, String Date_Added, String author_name, String ClassNames)
    {
        this.ID = id;
        this.Name = Name;
        this.Description = Description;
        this.Date_Added = Date_Added;
        this.author_name = author_name;
        this.ClassNames = ClassNames;
    }
    public int getID() { return ID; }

    public void setID(int ID) { this.ID = ID; }

    public String getClassNames() { return ClassNames; }

    public void setClassNames(String classNames) { ClassNames = classNames; }

    public String getName() {
        return Name;
    }

    public String getDescription() {
        return Description;
    }

    public String getDate_Added() {
        return Date_Added;
    }

    public String getAuthor_name() {
        return author_name;
    }

    public void setName(String name) {
        Name = name;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public void setDate_Added(String date_Added) {
        Date_Added = date_Added;
    }

    public void setAuthor_name(String author_name) {
        this.author_name = author_name;
    }
}
