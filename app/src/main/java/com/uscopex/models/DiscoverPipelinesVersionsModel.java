package com.uscopex.models;

public class DiscoverPipelinesVersionsModel {

    private int iD;
    private String  version;
    private String description;
    private String date;
    private int producingImage;
    private int producingResults;
    private int requiresRoi;
    private int requiresScale;


    public String getVersion() {
        return version;
    }

    public String getDescription() {
        return description;
    }

    public String getDate() {
        return date;
    }

    public int getProducingImage() {
        return producingImage;
    }

    public int getProducingResults() {
        return producingResults;
    }

    public int getRequiresRoi() {
        return requiresRoi;
    }

    public int getRequiresScale() {
        return requiresScale;
    }
}
