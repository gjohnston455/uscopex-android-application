package com.uscopex.models;

/*
Custom PipelineProperties model class for holding pipeline version data acquired in com.uscopex.api calls
Model is mapped to IAM Api
 */
public class PipelineProperties {
    private String name;
    private String versionNumber;
    private int versionId;
    private boolean roi;
    private boolean scale;
    private boolean producingImage;
    private boolean producingResults;


    public PipelineProperties(String name, String versionNumber, int versionId, boolean roi, boolean scale, boolean producingImage,
                              boolean producingResults) {
        super();
        this.name = name;
        this.versionNumber = versionNumber;
        this.versionId = versionId;
        this.roi = roi;
        this.scale = scale;
        this.producingImage = producingImage;
        this.producingResults = producingResults;
    }

    public PipelineProperties() {
    }

    public int getVersionId() {
        return versionId;
    }

    public void setVersionId(int versionId) {
        this.versionId = versionId;
    }

    public boolean isProducingImage() {
        return producingImage;
    }

    public void setProducingImage(boolean producingImage) {
        this.producingImage = producingImage;
    }

    public boolean isProducingResults() {
        return producingResults;
    }

    public void setProducingResults(boolean producingResults) {
        this.producingResults = producingResults;
    }

    public String getVersionNumber() {
        return versionNumber;
    }

    public void setVersionNumber(String versionNumber) {
        this.versionNumber = versionNumber;
    }

    public boolean isRoi() {
        return roi;
    }

    public void setRoi(boolean roi) {
        this.roi = roi;
    }

    public boolean isScale() {
        return scale;
    }

    public void setScale(boolean scale) {
        this.scale = scale;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return this.getName() + "_" + this.getVersionNumber() + ".bsh";
    }

}
