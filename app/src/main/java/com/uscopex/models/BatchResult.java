package com.uscopex.models;

import com.google.gson.annotations.SerializedName;

public class BatchResult {

    @SerializedName("imagePath")
    private String imagePath;
    @SerializedName("resultPath")
    private String resultPath;

    public BatchResult(String imagePath, String resultPath) {
        super();
        this.imagePath = imagePath;
        this.resultPath = resultPath;
    }
    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getResultPath() {
        return resultPath;
    }

    public void setResultPath(String resultPath) {
        this.resultPath = resultPath;
    }
}
