package com.uscopex.api;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Client {

    private static final String URL = "http://ec2-35-153-180-248.compute-1.amazonaws.com/RAPI/public/";
    private Retrofit retrofit;
    private  static  Client clientInstance;
    private Client(){
        retrofit = new Retrofit.Builder().baseUrl(URL)
                .addConverterFactory(GsonConverterFactory.create()).build();
    }
    public static synchronized Client getClientInstance(){
        if(clientInstance == null){
            clientInstance = new Client();
        }
        return clientInstance;
    }
    public RestAPI getRestAPI(){
        return retrofit.create(RestAPI.class);
    }
}
