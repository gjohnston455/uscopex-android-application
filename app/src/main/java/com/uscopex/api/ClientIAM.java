package com.uscopex.api;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class ClientIAM {

    private static final String URL = "http://192.168.0.22:8181/";
    //private static final String URL = "http://ec2-35-153-180-248.compute-1.amazonaws.com:8181/";
    //private static final String URL ="http://ec2-18-130-231-153.eu-west-2.compute.amazonaws.com:8181/";
    //private static final String URL = "http://ec2-3-8-186-95.eu-west-2.compute.amazonaws.com:8181";
    private Retrofit retrofit;
    private static ClientIAM clientInstance;

    /**
     * IAM Proccesses can take a long time due to image size and potential process complexity.
     * Due to this we need to expand the time beyond the default of 10s
     * This requires configuring OkHttp, Retrofits network layer.
     * This will allow us to get the correct response from the IAM API and not a timeout error.
     */
    private ClientIAM() {
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(3, TimeUnit.MINUTES)
                .readTimeout(3, TimeUnit.MINUTES)
                .writeTimeout(13, TimeUnit.MINUTES)
                .build();
        retrofit = new Retrofit.Builder().baseUrl(URL).client(okHttpClient).
                addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }
    public static synchronized ClientIAM getClientInstance() {
        if (clientInstance == null) {
            clientInstance = new ClientIAM();
        }
        return clientInstance;
    }
    public RestAPI getRestAPI() {
        return retrofit.create(RestAPI.class);
    }
}
