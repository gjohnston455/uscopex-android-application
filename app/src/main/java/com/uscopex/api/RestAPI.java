package com.uscopex.api;

import com.uscopex.responses.AnalysisRequestResponse;
import com.uscopex.responses.AnalysisResponse;
import com.uscopex.responses.BatchRequestResponse;
import com.uscopex.responses.BatchResponse;
import com.uscopex.responses.ClassificationResponse;

import com.uscopex.models.BatchROI;
import com.uscopex.models.BatchScale;
import com.uscopex.models.PipelineProperties;
import com.uscopex.models.ROI;

import com.uscopex.responses.DiscoverMLModelsResponse;
import com.uscopex.responses.DiscoverPipelineVersionResponse;
import com.uscopex.responses.DiscoverPipelinesResponse;
import com.uscopex.responses.IAMResultPathsResponse;
import com.uscopex.responses.ICMResultSetResponse;
import com.uscopex.responses.ResponseLogin;

import com.uscopex.models.Scale;
import com.uscopex.responses.IAMResultSetResponse;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
public interface RestAPI {

    @FormUrlEncoded
    @POST("createuser")
    Call<ResponseBody> newUser(
            @Field("FirstName") String firstName,
            @Field("LastName") String lastName,
            @Field("Email") String userEmail,
            @Field("Passwd") String userPasswd
    );
    @FormUrlEncoded
    @POST("login")
    Call<ResponseLogin> login(
            @Field("Email") String userEmail,
            @Field("Passwd") String userPasswd
    );
    @FormUrlEncoded
    @POST("pipeline")
    Call<String> pipeline(
            @Field("login_id") int login_id
    );
    @FormUrlEncoded
    @POST("batchpipeline")
    Call<String> batchPipeline(
            @Field("login_id") int login_id
    );

    @FormUrlEncoded
    @POST("pipelineversions")
    Call<String> pipelineVersions(
            @Field("pipeline_id") int selectedPipelineID
    );


    @FormUrlEncoded
    @POST("model")
    Call<String> model(
            @Field("login_id") int login_id
    );

    @FormUrlEncoded
    @POST("modelversions")
    Call<String> modelVersions(
            @Field("model_id") int selectedModel
    );

    @Multipart
    @POST("/classify")
    Call<ClassificationResponse> classification(
            @Part("user_id") RequestBody login_id,
            @Part("model_name") RequestBody modelName,
            @Part MultipartBody.Part imagefile ,
            @Part("classification_name") RequestBody classificationName,
            @Part("description") RequestBody description,
            @Part("model_id") RequestBody modelId
    );

    @FormUrlEncoded
    @POST("batchrequest")
    Call<BatchRequestResponse> batchRequest(
            @Field("process_name") String processName,
            @Field("pipeline") int pipeline,
            @Field("description") String description,
            @Field("user_id") int userId
    );

    @Multipart
    @POST("/batchprocess")
   Call<BatchResponse> batchProcess(
           @Part MultipartBody.Part[] img,
           @Part("processName") String processName,
           @Part("pipeline_properties") PipelineProperties pip,
           @Part("insertId") int insertId,
           @Part("userId") int userId
    );

    @Multipart
    @POST("/advancedbatchprocess")
    Call<BatchResponse> advancedBatchProcess(
            @Part MultipartBody.Part[] img,
            @Part("processName") String processName,
            @Part("pipeline_properties") PipelineProperties pip,
            @Part("insertId") int insertId,
            @Part("userId") int userId,
            @Part("rois[]") BatchROI roi,
            @Part("scale[]") BatchScale scale
    );

    @Multipart
    @POST("/simpleanalysis")
    Call<AnalysisResponse> simpleAnalysis(
            @Part MultipartBody.Part img,
            @Part("processName") String processName,
            @Part("description") String description,
            @Part("userId") int userId,
            @Part("pipeline_properties") PipelineProperties pip);

    @Multipart
    @POST("/advancedprocess")
    Call<AnalysisResponse> advancedAnalysis(
            @Part MultipartBody.Part img,
            @Part("processName") String processName,
            @Part("description") String description,
            @Part("userId") int userId,
            @Part("pipeline_properties") PipelineProperties pip,
            @Part("roi") ROI roi,
            @Part("scale") Scale scale
    );

    @FormUrlEncoded
    @POST("analysisnamevalidation")
    Call<AnalysisRequestResponse> analysisRequestValidation(
            @Field("process_name") String processName,
            @Field("user_id") int userId
    );

    @FormUrlEncoded
    @POST("icmresults")
    Call<ICMResultSetResponse> icmResults(
            @Field("user_id") int loginId
    );

    @FormUrlEncoded
    @POST("iamresults")
    Call<IAMResultSetResponse> resultSets(
            @Field("user_id") int loginId,
             @Field("is_batch") int isBatch
    );

    @FormUrlEncoded
    @POST("batchresultpaths")
    Call<IAMResultPathsResponse> batchResultPaths(
            @Field("result_set_id") int resultSetId
    );

    @FormUrlEncoded
    @POST("discovermodels")
    Call<DiscoverMLModelsResponse> discoverModels(
            @Field("user_id") int userId
    );

    @FormUrlEncoded
    @POST("discoverpipelines")
    Call<DiscoverPipelinesResponse> discoverPipelines(
            @Field("user_id") int userId
    );

    @FormUrlEncoded
    @POST("discoverpipelineversions")
    Call<DiscoverPipelineVersionResponse> discoverPipelineVersions(
            @Field("pipeline_id") int pipelineId
    );

    @FormUrlEncoded
    @POST("changeemail")
    Call<ResponseBody> changeEmail(
            @Field("email") String email,
             @Field("user_id") int user_id,
            @Field("passwd") String passwd
    );

    @FormUrlEncoded
    @POST("changepasswd")
    Call<ResponseBody> changePassword(
            @Field("current_passwd") String currentPasswd,
            @Field("new_passwd") String newPasswd,
            @Field("user_id") int user_id
    );
}
