package com.uscopex.api;

import retrofit2.Retrofit;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class ClientScalar {

    private static final String URL = "http://ec2-35-153-180-248.compute-1.amazonaws.com/RAPI/public/";
    private Retrofit retrofit;
    private  static ClientScalar clientInstance;
    private ClientScalar(){
        retrofit = new Retrofit.Builder().baseUrl(URL)
                .addConverterFactory(ScalarsConverterFactory.create()).build();
    }
    public static synchronized ClientScalar getClientInstance(){
        if(clientInstance == null){
            clientInstance = new ClientScalar();
        }
        return clientInstance;
    }
    public RestAPI getRestAPI(){
        return retrofit.create(RestAPI.class);
    }
}
