package com.uscopex.api;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ClientICM {

    private static final String URL = "http://192.168.0.22:5000/";
    private Retrofit retrofit;
    private static ClientICM clientInstance;

    private ClientICM() {
        retrofit = new Retrofit.Builder().baseUrl(URL)
                .addConverterFactory(GsonConverterFactory.create()).build();
    }
    public static synchronized ClientICM getClientInstance() {
        if (clientInstance == null) {
            clientInstance = new ClientICM();
        }
        return clientInstance;
    }
    public RestAPI getRestAPI() {
        return retrofit.create(RestAPI.class);
    }
}
