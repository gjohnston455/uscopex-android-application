package com.uscopex.storage;

import android.content.Context;
import android.content.SharedPreferences;

import com.uscopex.models.User;

public class UserManager {

    private static final String USER_MANAGER_NAME = "user_manager";
    private static UserManager instance;

    private Context context;

    private UserManager(Context context){
        this.context = context;
    }

    public static synchronized UserManager getInstance(Context context){
        if(instance == null){
            instance = new UserManager(context);
        }
        return instance;
    }
    public void save(User user){
        SharedPreferences sharedPreferences = context.getSharedPreferences(USER_MANAGER_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putInt("accountId", user.getAccountId());
        editor.putInt("loginId", user.getLoginId());
        editor.putString("firstName", user.getFirstName());
        editor.putString("lastName", user.getLastName());
        editor.putString("email", user.getEmail());
        editor.apply();
    }
    public User getUserAccount(){
        SharedPreferences sharedPreferences = context.getSharedPreferences(USER_MANAGER_NAME, Context.MODE_PRIVATE);
        User user = new User(sharedPreferences.getInt("accountId",-1),
                sharedPreferences.getString("firstName",null),
                sharedPreferences.getString("lastName",null),
                sharedPreferences.getString("email",null),
                sharedPreferences.getInt("loginId",-1)
        );
        return user;

    }
    public boolean isUserLoggedIn(){
        SharedPreferences sharedPreferences = context.getSharedPreferences(USER_MANAGER_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getInt("loginId",-1)!=-1;
    }
    public void logOut(){
        SharedPreferences sharedPreferences = context.getSharedPreferences(USER_MANAGER_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.apply();
    }
}
