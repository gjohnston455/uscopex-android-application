package com.uscopex.responses;

import java.util.List;

import com.uscopex.models.DiscoverMLModelsModel;

public class DiscoverMLModelsResponse {

    private boolean error;
    private List<DiscoverMLModelsModel> results;

    public DiscoverMLModelsResponse(){}

    public DiscoverMLModelsResponse(boolean error, List<DiscoverMLModelsModel> resultSets) {
        this.error = error;
        this.results = resultSets;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public List<DiscoverMLModelsModel> getResults() {
        return results;
    }

    public void setResults(List<DiscoverMLModelsModel> results) {
        this.results = results;
    }
}
