package com.uscopex.responses;

import java.util.List;

import com.uscopex.models.ICMResultSet;

public class ICMResultSetResponse {

    private boolean error;
    private List<ICMResultSet> results;

    public ICMResultSetResponse(){}

    public ICMResultSetResponse(boolean error, List<ICMResultSet> resultSets) {
        this.error = error;
        this.results = resultSets;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public List<ICMResultSet> getResults() {
        return results;
    }

    public void setResults(List<ICMResultSet> results) {
        this.results = results;
    }
}
