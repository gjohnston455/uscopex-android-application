package com.uscopex.responses;

public class ClassificationResponse {

    private boolean error;
    private String message;
    private int code;

    public ClassificationResponse(boolean error, String message, int code) {
        this.error = error;
        this.message = message;
        this.code = code;
    }

    public boolean isError() {
        return error;
    }
    public String getMessage() {
        return message;
    }
    public int getCode() {
        return code;
    }
}


