package com.uscopex.responses;

public class BatchRequestResponse {

    private boolean error;
    private String message;
    private int request_id;

    public BatchRequestResponse(boolean error, String message, int request_id) {
        this.error = error;
        this.message = message;
        this.request_id = request_id;
    }

    public boolean isError() {
        return error;
    }

    public String getMessage() {
        return message;
    }

    public int getRequest_id() {
        return request_id;
    }
}
