package com.uscopex.responses;

import java.util.List;

import com.uscopex.models.DiscoverPipelinesVersionsModel;

public class DiscoverPipelineVersionResponse {

    private boolean error;
    private List<DiscoverPipelinesVersionsModel> results;

    public DiscoverPipelineVersionResponse(){}

    public DiscoverPipelineVersionResponse(boolean error, List<DiscoverPipelinesVersionsModel> resultSets) {
        this.error = error;
        this.results = resultSets;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public List<DiscoverPipelinesVersionsModel> getResults() {
        return results;
    }

    public void setResults(List<DiscoverPipelinesVersionsModel> results) {
        this.results = results;
    }
}
