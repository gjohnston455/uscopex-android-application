package com.uscopex.responses;

import java.util.List;

import com.uscopex.models.IAMResultSet;

public class IAMResultSetResponse {

    private boolean error;
    private List<IAMResultSet> results;

    public IAMResultSetResponse(){}

    public IAMResultSetResponse(boolean error, List<IAMResultSet> resultSets) {
        this.error = error;
        this.results = resultSets;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public List<IAMResultSet> getResults() {
        return results;
    }

    public void setResults(List<IAMResultSet> results) {
        this.results = results;
    }
}
