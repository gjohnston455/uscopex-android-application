package com.uscopex.responses;

public class IAMResultPathsResponse {


    private boolean error;
    private ImagePath[] imagePaths;
    private ResultPath[] resultPaths;


    public boolean isError() {
        return error;
    }

    public ImagePath[] getImagePaths() {
        return imagePaths;
    }

    public ResultPath[] getResultPaths() {
        return resultPaths;
    }

    public class ImagePath {
        private String imagePath;
        public String getImagePath() {
            return imagePath;
        }
    }

    public class ResultPath {
        private String resultPath;
        public String getResultPath() {
            return resultPath;
        }
    }
}
