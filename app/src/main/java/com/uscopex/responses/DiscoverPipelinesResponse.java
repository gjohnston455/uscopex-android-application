package com.uscopex.responses;

import java.util.List;

import com.uscopex.models.DiscoverPipelinesModel;

public class DiscoverPipelinesResponse {

    private boolean error;
    private List<DiscoverPipelinesModel> results;

    public DiscoverPipelinesResponse(){}

    public DiscoverPipelinesResponse(boolean error, List<DiscoverPipelinesModel> resultSets) {
        this.error = error;
        this.results = resultSets;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public List<DiscoverPipelinesModel> getResults() {
        return results;
    }

    public void setResults(List<DiscoverPipelinesModel> results) {
        this.results = results;
    }
}
