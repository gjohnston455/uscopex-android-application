package com.uscopex.ImageModifier;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

public class ScaleCalculator extends View {

    private Paint rectanglePaint;
    private int xStart;
    private int yStart;
    private int xEnd;
    private int yEnd;
    private boolean drawingInProcess;

    private RectangleCompletedCallback rectangleCompletedCallback;

    public void setRectangleCompletedCallback(RectangleCompletedCallback rectangleCompletedCallback) {
        this.rectangleCompletedCallback = rectangleCompletedCallback;
    }

    /*
    Multiple constructors for initialising the view.
    These are required by extending view.
     */
    public ScaleCalculator(final Context context) {
        super(context);
        createPaint();
    }

    public ScaleCalculator(final Context context, final AttributeSet attrs) {
        super(context, attrs);
        createPaint();
    }

    public ScaleCalculator(final Context context, final AttributeSet attrs, final int defStyle) {
        super(context, attrs, defStyle);
        createPaint();
    }

    private void createPaint() {
        rectanglePaint = new Paint();
        rectanglePaint.setColor(getContext().getResources().getColor(android.R.color.holo_green_light));
        rectanglePaint.setStyle(Paint.Style.STROKE);
        rectanglePaint.setStrokeWidth(6);
    }

    @Override
    public boolean onTouchEvent(final MotionEvent event) {

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                drawingInProcess = false;
                xStart = (int) event.getX();
                yStart = (int) event.getY();
                invalidate();
                break;

            case MotionEvent.ACTION_MOVE:
                final int x = (int) event.getX();
                final int y = (int) event.getY();
                if (!drawingInProcess || Math.abs(x - xEnd) > 1 || Math.abs(y - yEnd) > 1) {
                    xEnd = x;
                    yEnd = y;
                    invalidate();
                }
                drawingInProcess = true;
                break;

            case MotionEvent.ACTION_UP:

                if (rectangleCompletedCallback != null) {
                    rectangleCompletedCallback.rectangleCompleted(
                            new Rect(Math.min(xStart, xEnd), Math.min(yStart, yEnd),
                                    Math.max(xEnd, xStart), Math.max(yEnd, xStart)));
                    rectangleCompletedCallback.widthHeightArr(
                            new int[]{Math.abs(xStart - xEnd), Math.abs(yStart - yEnd)});
                }
                invalidate();
                break;
            default:
                break;
        }
        return true;
    }

    /**
     * While MovementEvent.ACTION_MOVE is triggered drawingInProcess = true
     * The canvas is drawn and a rectangle is drawn using the private global coordinate values.
     * Rectangle always uses the minimum values as top left x,y and max for bottom x,y
     * This method is repeatably called by the invalidate() method.
     *
     * @param canvas
     */
    @Override
    protected void onDraw(final Canvas canvas) {
        super.onDraw(canvas);
        if (drawingInProcess) {
            canvas.drawARGB(0, 0, 0, 0);
            canvas.drawRect(
                    Math.min(xStart, xEnd), Math.min(yStart, yEnd),
                    Math.max(xEnd, xStart), Math.max(yEnd, yStart),
                    rectanglePaint);
        }
    }
}