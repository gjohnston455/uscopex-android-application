package com.uscopex.ImageModifier;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import com.gareth.R;

public class SetScaleActivity extends Activity  {


    private int roiArray [] = new int[4];
    int calculatedDistance;
    Bitmap bitmapImage;
    ImageView imageView;
    float imageWidth, imageHeight;
    boolean scaleSet;
    float imageViewWidth, imageViewHeight;
    float widthRatio, heightRatio;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.image_modifiers_view);

        final ScaleCalculator scaleView = findViewById(R.id.dragRect);
        bitmapImage =  BitmapFactory.decodeFile(getIntent().getStringExtra("image_path"));

        imageWidth = bitmapImage.getWidth();
        imageHeight = bitmapImage.getHeight();
        imageView = findViewById(R.id.imageView10);
        imageView.setImageBitmap(bitmapImage);


        /*
        Callback using interface
         */
        if(scaleView!=null){
            scaleView.setRectangleCompletedCallback(new RectangleCompletedCallback() {
                @Override
                public void rectangleCompleted(Rect rect) { }

                @Override
                public void widthHeightArr(int[] widthHeightArray) {
                    imageViewWidth = imageView.getWidth();
                    imageViewHeight = imageView.getHeight();
                    widthRatio = imageWidth/imageViewWidth;
                    heightRatio = imageHeight/imageViewHeight;

                    float scaledWidth  = widthRatio * (float) widthHeightArray[0];
                    float scaledHeight = heightRatio *(float) widthHeightArray[1];

                    roiArray[2] = (int) scaledWidth;
                    roiArray[3] = (int) scaledHeight;
                    calculatedDistance = Math.max(roiArray[2], roiArray[3]);
                }
            });
        }
    }

   /*
   No scale set
    */
    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        setResult(RESULT_OK, intent);
        super.onBackPressed();
    }

    /*
    Volume key events for returning to calling activity.
     */
    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        int action = event.getAction();
        int keyCode = event.getKeyCode();
        switch (keyCode) {
            case KeyEvent.KEYCODE_VOLUME_UP:
                if (action == KeyEvent.ACTION_DOWN) {
                    scaleSet = true;
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra("distance",calculatedDistance);
                    returnIntent.putExtra("scaleSet",scaleSet);
                    onActivityResult(3, 15, returnIntent);
                    setResult(SetScaleActivity.RESULT_OK, returnIntent);
                    finish();
                }
                return true;
            case KeyEvent.KEYCODE_VOLUME_DOWN:
                if (action == KeyEvent.ACTION_DOWN || action == KeyEvent.KEYCODE_BACK) {
                    scaleSet = false;
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra("scaleSet",scaleSet);
                    onActivityResult(3, 15, returnIntent);
                    setResult(SetScaleActivity.RESULT_OK, returnIntent);
                    finish();
                }
                return true;
            default:
                return super.dispatchKeyEvent(event);
        }
    }
}