package com.uscopex.ImageModifier;

import android.graphics.Rect;

public interface RectangleCompletedCallback {

    void rectangleCompleted(Rect rect);

    void widthHeightArr(int[] widthHeightArray);
}
