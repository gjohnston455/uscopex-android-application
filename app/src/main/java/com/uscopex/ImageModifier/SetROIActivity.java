package com.uscopex.ImageModifier;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import com.gareth.R;

public class SetROIActivity extends Activity {


    private int roiArray[] = new int[4];
    Bitmap bitmapImage;
    ImageView view;
    private float imageWidth, imageHeight;
    boolean roiSet;

    int x1, x2, y1, y2;
    float imageViewWidth, imageViewHeight;
    float widthRatio, heightRatio;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.image_modifiers_view);

        bitmapImage = BitmapFactory.decodeFile(getIntent().getStringExtra("image_path"));

        imageWidth = bitmapImage.getWidth();
        imageHeight = bitmapImage.getHeight();
        view = findViewById(R.id.imageView10);
        view.setImageBitmap(bitmapImage);

        final ScaleCalculator scaleCalculatorView = findViewById(R.id.dragRect);
        /*
        Callback using interface
         */
        if (scaleCalculatorView != null) {
            scaleCalculatorView.setRectangleCompletedCallback(new RectangleCompletedCallback() {
                @Override
                public void rectangleCompleted(Rect rect) {
                    x1 = rect.left;
                    y1 = rect.top;
                    x2 = rect.right;
                    y2 = rect.bottom;
                    final float[] coordinates = new float[]{x1, y1, x2, y2};
                    Matrix matrix = new Matrix();
                    view.getImageMatrix().invert(matrix);
                    matrix.mapPoints(coordinates);
                    roiArray[0] = (int) coordinates[0];
                    roiArray[1] = (int) coordinates[1];
                }

                @Override
                public void widthHeightArr(int[] widthHeightArray) {
                    imageViewWidth = view.getWidth();
                    imageViewHeight = view.getHeight();
                    widthRatio = imageWidth / imageViewWidth;
                    heightRatio = imageHeight / imageViewHeight;
                    float scaledWidth = widthRatio * (float) widthHeightArray[0];
                    float scaledHeight = heightRatio * (float) widthHeightArray[1];
                    roiArray[2] = (int) scaledWidth;
                    roiArray[3] = (int) scaledHeight;
                }
            });
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        setResult(RESULT_OK, intent);
        super.onBackPressed();
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        int action = event.getAction();
        int keyCode = event.getKeyCode();
        switch (keyCode) {
            case KeyEvent.KEYCODE_VOLUME_UP:
                if (action == KeyEvent.ACTION_DOWN) {
                    roiSet = true;
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra("roiSet", roiSet);
                    returnIntent.putExtra("roi", roiArray);
                    onActivityResult(2, 2, returnIntent);
                    setResult(SetROIActivity.RESULT_CANCELED, returnIntent);
                    finish();
                }
                return true;
            case KeyEvent.KEYCODE_VOLUME_DOWN:
                if (action == KeyEvent.ACTION_DOWN) {
                    roiSet = false;
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra("roiSet", roiSet);
                    onActivityResult(2, 2, returnIntent);
                    setResult(SetROIActivity.RESULT_OK, returnIntent);
                    finish();
                }
                return true;

            default:
                return super.dispatchKeyEvent(event);
        }
    }
}