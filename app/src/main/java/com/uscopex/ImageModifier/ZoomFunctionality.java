package com.uscopex.ImageModifier;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;


import androidx.appcompat.app.AppCompatActivity;

import com.gareth.R;
import com.github.chrisbanes.photoview.PhotoView;

public class ZoomFunctionality extends AppCompatActivity {
    Bitmap image;
    PhotoView photoView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_zoom_functionality_layout);
        image = BitmapFactory.decodeFile(getIntent().getStringExtra("image_path"));
        photoView = findViewById(R.id.photo_view);
        photoView.setImageBitmap(image);
    }
}