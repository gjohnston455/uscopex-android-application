package com.uscopex.account;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.gareth.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import com.uscopex.api.Client;
import com.uscopex.main.HomeActivity;
import com.uscopex.storage.UserManager;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText firstNameEditT, lastNameEditT, emailEditT, passwordEditT;
    private String userEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.DarkTheme);
        setContentView(R.layout.signup_activity_layout);

        firstNameEditT = findViewById(R.id.firstNameEditT);
        lastNameEditT = findViewById(R.id.lastNameEditT);
        emailEditT= findViewById(R.id.emailEditT);
        passwordEditT= findViewById(R.id.passwordEditT);

        findViewById(R.id.buttonSignUp).setOnClickListener(this);
        findViewById(R.id.textViewLogin).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.buttonSignUp:
                signUp();
                break;
            case R.id.textViewLogin:
                startActivity(new Intent(this, LoginActivity.class));
                break;
        }
    }

    private void signUp() {
       String firstName = firstNameEditT.getText().toString().trim();
       String lastName = lastNameEditT.getText().toString().trim();
       userEmail = emailEditT.getText().toString().trim();
       String userPasswd = passwordEditT.getText().toString().trim();

       if(firstName.isEmpty()){
           firstNameEditT.setError("You must enter a name");
           firstNameEditT.requestFocus();
           return;
       }

        if (lastName.isEmpty()) {
            lastNameEditT.setError("You must enter a last name");
            lastNameEditT.requestFocus();
            return;
        }
        if(userEmail.isEmpty()){
            emailEditT.setError("You must enter an email");
            emailEditT.requestFocus();
            return;
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(userEmail).matches()) {
            emailEditT.setError("Enter a valid email");
            emailEditT.requestFocus();
            return;
        }

        if (userPasswd.isEmpty()) {
            passwordEditT.setError("Password required");
            passwordEditT.requestFocus();
            return;
        }
        if (userPasswd.length() < 6) {
            passwordEditT.setError("Password should be  6 character long");
            passwordEditT.requestFocus();
            return;
        }

        Call<ResponseBody> createUserCall = Client.getClientInstance()
                .getRestAPI()
                .newUser(firstName,lastName,userEmail,userPasswd);
        createUserCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                String serverResponse = null;
                if(response.code()==201) {
                    try {
                         serverResponse = response.body().string();
                         directToHome();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }else{
                    try {
                        serverResponse = response.errorBody().string();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if(!serverResponse.isEmpty()){
                    try {
                        JSONObject json = new JSONObject(serverResponse);
                        Toast.makeText(SignUpActivity.this, json.getString("message"), Toast.LENGTH_LONG).show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(SignUpActivity.this,"Unable to establish connection to server",Toast.LENGTH_LONG).show();
            }
        });
    }
    private void directToHome(){
        Intent intent = new Intent(SignUpActivity.this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        intent.putExtra("email",userEmail);
    }
}