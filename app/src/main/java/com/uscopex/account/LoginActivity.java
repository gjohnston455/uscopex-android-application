package com.uscopex.account;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.uscopex.main.HomeActivity;
import com.gareth.R;

import com.uscopex.api.Client;
import com.uscopex.responses.ResponseLogin;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import com.uscopex.storage.UserManager;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText  emailEditT, passwordEditT;
    private boolean fromSignUpIntent;
    private String email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.DarkTheme);
        setContentView(R.layout.activity_login);

        emailEditT= findViewById(R.id.emailEditT);
        passwordEditT= findViewById(R.id.passwordEditT);

        findViewById(R.id.buttonSignIn).setOnClickListener(this);
        findViewById(R.id.textViewRegister).setOnClickListener(this);
        if(getIntent()!=null) {
            Intent fromSignUp = getIntent();
            fromSignUpIntent = true;
            email = fromSignUp.getStringExtra("email");
            emailEditT.setText(email);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if(UserManager.getInstance(this).isUserLoggedIn()){
            Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.buttonSignIn:
                signIn();
                break;
            case R.id.textViewRegister:
                startActivity(new Intent(this, SignUpActivity.class));
                break;
        }
    }

    private void signIn() {
       String userEmail = emailEditT.getText().toString().trim();
       String userPasswd = passwordEditT.getText().toString().trim();


        if(userEmail.isEmpty()){
            emailEditT.setError("You must enter an email");
            emailEditT.requestFocus();
            return;
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(userEmail).matches()) {
            emailEditT.setError("Enter a valid email");
            emailEditT.requestFocus();
            return;
        }

        if (userPasswd.isEmpty()) {
            passwordEditT.setError("Password required");
            passwordEditT.requestFocus();
            return;
        }
        if (userPasswd.length() < 6) {
            passwordEditT.setError("Password should be  6 character long");
            passwordEditT.requestFocus();
            return;
        }
        Call<ResponseLogin> call = Client.getClientInstance().getRestAPI().login(userEmail,userPasswd);
        call.enqueue(new Callback<ResponseLogin>() {
            @Override
            public void onResponse(Call<ResponseLogin> call, Response<ResponseLogin> response) {

                ResponseLogin responseLogin = response.body();

                if(responseLogin != null){
                    UserManager.getInstance(LoginActivity.this).save(responseLogin.getUser());
                    Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }else{
                    Log.d("errormsg", "User doesn't exist");
                    Toast.makeText(LoginActivity.this,"User doesn't exist",Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseLogin> call, Throwable t) {
                Toast.makeText(LoginActivity.this,"Unable to establish connect to server ",Toast.LENGTH_SHORT).show();
            }
        });
    }
}