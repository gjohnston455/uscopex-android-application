package com.uscopex.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.gareth.R;

import java.util.ArrayList;
import java.util.List;

import com.uscopex.models.DiscoverMLModelsModel;

public class DiscoverMLModelsAdapter extends RecyclerView.Adapter<DiscoverMLModelsAdapter.DiscoverMLModelsApdaterVH> implements Filterable {

    private List<DiscoverMLModelsModel> resultSetResponseList;
    private List<DiscoverMLModelsModel> resultSetResponseListCopyFull;
    private Context context;

    public void setResults(List<DiscoverMLModelsModel> resultSetResponseList) {
        this.resultSetResponseList = resultSetResponseList;
        resultSetResponseListCopyFull = new ArrayList<>(resultSetResponseList);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public DiscoverMLModelsApdaterVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context = parent.getContext();
        return new DiscoverMLModelsApdaterVH(LayoutInflater.from(context).inflate(R.layout.ml_model_info_row,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull DiscoverMLModelsApdaterVH holder, int position) {

        DiscoverMLModelsModel resultSetResponse = resultSetResponseList.get(position);
        String modelName = resultSetResponse.getName();
        String author = resultSetResponse.getAuthor();
        String description = resultSetResponse.getDescription();
        String classNames = resultSetResponse.getClassNames();

        holder.resultModelName.setText(modelName);
        holder.resultDescription.setText(description);
        holder.resAuthor.setText(author);
        holder.resClassNames.setText(classNames);
    }

    @Override
    public int getItemCount() {
        return resultSetResponseList.size();
    }

    @Override
    public Filter getFilter() {
        return resultSetFilter;
    }
    private Filter resultSetFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {
            List<DiscoverMLModelsModel> filteredList = new ArrayList<>();

            if(charSequence == null || charSequence.length()==0){
                filteredList.addAll(resultSetResponseListCopyFull);
            }
            String filterSequence = charSequence.toString().toLowerCase().trim();
            for(DiscoverMLModelsModel item : resultSetResponseListCopyFull){
                if(item.getName().toLowerCase().contains(filterSequence)){
                    filteredList.add(item);
                }
            }
            FilterResults results = new FilterResults();
            results.values = filteredList;
            return  results;
        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            resultSetResponseList.clear();
            resultSetResponseList.addAll(((List)filterResults.values));
            notifyDataSetChanged();
        }
    };

    public class DiscoverMLModelsApdaterVH extends RecyclerView.ViewHolder {
        TextView resultModelName, resultDescription,resAuthor, resClassNames;
        public DiscoverMLModelsApdaterVH(@NonNull View itemView) {
            super(itemView);
            resultModelName = itemView.findViewById(R.id.resModelName);
            resultDescription = itemView.findViewById(R.id.resModelDescription);
            resAuthor = itemView.findViewById(R.id.resModelAuthor);
            resClassNames = itemView.findViewById(R.id.resModelClassNames);
        }
    }
}
