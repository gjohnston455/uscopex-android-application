package com.uscopex.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import com.gareth.R;
import com.uscopex.models.IAMResultSet;

public class IAMResultsAdapter extends RecyclerView.Adapter<IAMResultsAdapter.ResultApdaterVH> implements Filterable {

    private List<IAMResultSet> resultSetResponseList;
    private List<IAMResultSet> resultSetResponseListCopyFull;
    private Context context;
    private ClickedButtonListener clickedButtonListener;

    public IAMResultsAdapter(ClickedButtonListener clickedButtonListener) {
        this.clickedButtonListener = clickedButtonListener;
    }
    public void setResults(List<IAMResultSet> resultSetResponseList) {
        this.resultSetResponseList = resultSetResponseList;
        resultSetResponseListCopyFull = new ArrayList<>(resultSetResponseList);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ResultApdaterVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context = parent.getContext();
        return new IAMResultsAdapter.ResultApdaterVH(LayoutInflater.from(context).inflate(R.layout.iam_result_set_row,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull ResultApdaterVH holder, int position) {

        IAMResultSet resultSetResponse = resultSetResponseList.get(position);
        String analysisName = resultSetResponse.getName();
        String date = resultSetResponse.getDate();
        String description = resultSetResponse.getDescription();
        String pipeline = resultSetResponse.getPipeline() + " V"+resultSetResponse.getPipelineVersion();


        holder.resultName.setText(analysisName);
        holder.resultDate.setText(date);
        holder.resultDescription.setText(description);
        holder.resultPipeline.setText(pipeline);
        holder.goToResult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickedButtonListener.clickedResult(resultSetResponse);
            }
        });
    }

    public interface ClickedButtonListener{
        public void clickedResult(IAMResultSet resultSetResponse);
    }

    @Override
    public int getItemCount() {
        return resultSetResponseList.size();
    }

    @Override
    public Filter getFilter() {
        return resultSetFilter;
    }
    private Filter resultSetFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {
            List<IAMResultSet> filteredList = new ArrayList<>();

            if(charSequence == null || charSequence.length()==0){
                filteredList.addAll(resultSetResponseListCopyFull);
            }
            String filterSequence = charSequence.toString().toLowerCase().trim();
            for(IAMResultSet item : resultSetResponseListCopyFull){
                if(item.getName().toLowerCase().contains(filterSequence)){
                    filteredList.add(item);
                }
            }
            FilterResults results = new FilterResults();
            results.values = filteredList;
            return  results;
        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            resultSetResponseList.clear();
            resultSetResponseList.addAll(((List)filterResults.values));
            notifyDataSetChanged();
        }
    };

    public class ResultApdaterVH extends RecyclerView.ViewHolder {

        TextView resultName, resultDate, resultDescription, resultPipeline;
        ImageView goToResult;
        public ResultApdaterVH(@NonNull View itemView) {
            super(itemView);
            resultName = itemView.findViewById(R.id.resAnalysisName);
            resultDate = itemView.findViewById(R.id.resAnalysisDate);
            resultDescription = itemView.findViewById(R.id.resAnalysisDescription);
            resultPipeline = itemView.findViewById(R.id.resPipeline);
            goToResult = itemView.findViewById(R.id.goToResult);
        }
    }
}
