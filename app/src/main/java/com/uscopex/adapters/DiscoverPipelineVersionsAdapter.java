package com.uscopex.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.gareth.R;
import com.uscopex.models.DiscoverPipelinesVersionsModel;

import java.util.ArrayList;
import java.util.List;

public class DiscoverPipelineVersionsAdapter extends RecyclerView.Adapter<DiscoverPipelineVersionsAdapter.DiscoverPipelinesApdaterVH>  {

    private List<DiscoverPipelinesVersionsModel> resultSetResponseList;
    private List<DiscoverPipelinesVersionsModel> resultSetResponseListCopyFull;
    private Context context;
    private ClickedButtonListener clickedButtonListener;

    public DiscoverPipelineVersionsAdapter(ClickedButtonListener clickedButtonListener) {
        this.clickedButtonListener = clickedButtonListener;
    }
    public void setResults(List<DiscoverPipelinesVersionsModel> resultSetResponseList) {
        this.resultSetResponseList = resultSetResponseList;
        resultSetResponseListCopyFull = new ArrayList<>(resultSetResponseList);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public DiscoverPipelinesApdaterVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context = parent.getContext();
        return new DiscoverPipelinesApdaterVH(LayoutInflater.from(context).inflate(R.layout.pipeline_version_view_row,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull DiscoverPipelinesApdaterVH holder, int position) {

        DiscoverPipelinesVersionsModel resultSetResponse = resultSetResponseList.get(position);

        String version = resultSetResponse.getVersion();

        int requiresRoi = resultSetResponse.getRequiresRoi();
        int requiresScale = resultSetResponse.getRequiresScale();
        int outputsImage = resultSetResponse.getProducingImage();
        int outputsResults = resultSetResponse.getProducingResults();
        String description = resultSetResponse.getDescription();
        String date = resultSetResponse.getDate();
        String requirements = "No special requirements";
        if(requiresRoi >0 && requiresScale<1)requirements="ROI";
        if(requiresScale >0 && requiresRoi<1)requirements= "Scale";
        if(requiresRoi>0 && requiresScale>01)requirements = "ROI & Scale";
        String outputs ="";
        if(outputsImage >0 && outputsResults<1)outputs="Processed Image ";
        if(outputsResults >0 && outputsImage<1)outputs= "Result Set";
        if(outputsResults >0 && outputsImage>0)outputs = "Processed Image & Result Set";

        holder.resVersionNumber.setText(version);
        holder.resultDescription.setText(description);
        holder.resultRequirements.setText(requirements);
        holder.resVersionDate.setText(date);
        holder.resOutputs.setText(outputs);

    }

    public interface ClickedButtonListener{
        void clickedResult(DiscoverPipelinesVersionsModel resultSetResponse);
    }

    @Override
    public int getItemCount() {
        return resultSetResponseList.size();
    }


    public class DiscoverPipelinesApdaterVH extends RecyclerView.ViewHolder {

        TextView resVersionNumber, resVersionDate, resultDescription, resultRequirements, resOutputs;

        public DiscoverPipelinesApdaterVH(@NonNull View itemView) {
            super(itemView);
            resVersionNumber = itemView.findViewById(R.id.resVersionNumber);
            resVersionDate = itemView.findViewById(R.id.resPipelineVersionDate);
            resultDescription = itemView.findViewById(R.id.resPipelineVersionDescription);
            resultRequirements = itemView.findViewById(R.id.resPipelineRequirements);
            resOutputs = itemView.findViewById(R.id.resPipelineOutputs);
        }
    }
}
