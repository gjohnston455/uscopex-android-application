package com.uscopex.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.gareth.R;
import com.uscopex.models.DiscoverPipelinesModel;

import java.util.ArrayList;
import java.util.List;

public class DiscoverPipelinesAdapter extends RecyclerView.Adapter<DiscoverPipelinesAdapter.DiscoverPipelinesApdaterVH> implements Filterable {

    private List<DiscoverPipelinesModel> resultSetResponseList;
    private List<DiscoverPipelinesModel> resultSetResponseListCopyFull;
    private Context context;
    private ClickedButtonListener clickedButtonListener;

    public DiscoverPipelinesAdapter(ClickedButtonListener clickedButtonListener) {
        this.clickedButtonListener = clickedButtonListener;
    }
    public void setResults(List<DiscoverPipelinesModel> resultSetResponseList) {
        this.resultSetResponseList = resultSetResponseList;
        resultSetResponseListCopyFull = new ArrayList<>(resultSetResponseList);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public DiscoverPipelinesApdaterVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context = parent.getContext();
        return new DiscoverPipelinesApdaterVH(LayoutInflater.from(context).inflate(R.layout.pipeline_view_row,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull DiscoverPipelinesApdaterVH holder, int position) {

        DiscoverPipelinesModel resultSetResponse = resultSetResponseList.get(position);
        String pipelineName = resultSetResponse.getName();
        int versions = resultSetResponse.getVersions();
        String author = resultSetResponse.getAuthor();
        int batchable = resultSetResponse.isBatchable();
        String description = resultSetResponse.getDescription();


        holder.resultPipelineName.setText(pipelineName);
        holder.resultVersions.setText(Integer.toString(versions));
        holder.resultDescription.setText(description);
        holder.resAuthor.setText(author);
        Log.i("variable ", pipelineName);
        if(batchable>0)holder.resBatchReady.setText("Batch ready");


        holder.goToMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickedButtonListener.clickedResult(resultSetResponse);
            }
        });
    }

    public interface ClickedButtonListener{
        void clickedResult(DiscoverPipelinesModel resultSetResponse);
    }

    @Override
    public int getItemCount() {
        return resultSetResponseList.size();
    }

    @Override
    public Filter getFilter() {
        return resultSetFilter;
    }
    private Filter resultSetFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {
            List<DiscoverPipelinesModel> filteredList = new ArrayList<>();

            if(charSequence == null || charSequence.length()==0){
                filteredList.addAll(resultSetResponseListCopyFull);
            }
            String filterSequence = charSequence.toString().toLowerCase().trim();
            for(DiscoverPipelinesModel item : resultSetResponseListCopyFull){
                if(item.getName().toLowerCase().contains(filterSequence)){
                    filteredList.add(item);
                }
            }
            FilterResults results = new FilterResults();
            results.values = filteredList;
            return  results;
        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            resultSetResponseList.clear();
            resultSetResponseList.addAll(((List)filterResults.values));
            notifyDataSetChanged();
        }
    };

    public class DiscoverPipelinesApdaterVH extends RecyclerView.ViewHolder {

        TextView resultPipelineName, resultVersions, resultDescription,resAuthor, resBatchReady;
        ImageView goToMore;
        public DiscoverPipelinesApdaterVH(@NonNull View itemView) {
            super(itemView);
            resultPipelineName = itemView.findViewById(R.id.resPipelineName);
            resultVersions = itemView.findViewById(R.id.resPipelineVersion);
            resultDescription = itemView.findViewById(R.id.resPipelineDescription);
            resAuthor = itemView.findViewById(R.id.resAuthor);
            goToMore = itemView.findViewById(R.id.goToMore);
            resBatchReady = itemView.findViewById(R.id.batchReady2);
        }
    }
}
