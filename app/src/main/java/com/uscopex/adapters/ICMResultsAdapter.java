package com.uscopex.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.gareth.R;
import com.uscopex.models.ICMResultSet;

import java.util.ArrayList;
import java.util.List;

public class ICMResultsAdapter extends RecyclerView.Adapter<ICMResultsAdapter.ResultApdaterVH> implements Filterable {

    private List<ICMResultSet> resultSetResponseList;
    private List<ICMResultSet> resultSetResponseListCopyFull;
    private Context context;

    public void setResults(List<ICMResultSet> resultSetResponseList) {
        this.resultSetResponseList = resultSetResponseList;
        resultSetResponseListCopyFull = new ArrayList<>(resultSetResponseList);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ResultApdaterVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context = parent.getContext();
        return new ICMResultsAdapter.ResultApdaterVH(LayoutInflater.from(context).inflate(R.layout.icm_result_set_row,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull ResultApdaterVH holder, int position) {

        ICMResultSet resultSetResponse = resultSetResponseList.get(position);
        String classificationName = resultSetResponse.getName();
        String date = resultSetResponse.getDate();
        String description = resultSetResponse.getDescription();
        String model = resultSetResponse.getModelName();
        String classNamesArr [] = resultSetResponse.getClassNames().split(",");
        String classification = classNamesArr[resultSetResponse.getClassification()];

        holder.resultName.setText(classificationName);
        holder.resultDate.setText(date);
        holder.resultDescription.setText(description);
        holder.resultModel.setText(model);
        holder.resClassification.setText(classification);
    }

    @Override
    public int getItemCount() {
        return resultSetResponseList.size();
    }

    @Override
    public Filter getFilter() {
        return resultSetFilter;
    }
    private Filter resultSetFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {
            List<ICMResultSet> filteredList = new ArrayList<>();

            if(charSequence == null || charSequence.length()==0){
                filteredList.addAll(resultSetResponseListCopyFull);
            }
            String filterSequence = charSequence.toString().toLowerCase().trim();
            for(ICMResultSet item : resultSetResponseListCopyFull){
                if(item.getName().toLowerCase().contains(filterSequence)){
                    filteredList.add(item);
                }
            }
            FilterResults results = new FilterResults();
            results.values = filteredList;
            return  results;
        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            resultSetResponseList.clear();
            resultSetResponseList.addAll(((List)filterResults.values));
            notifyDataSetChanged();
        }
    };

    public class ResultApdaterVH extends RecyclerView.ViewHolder {
        TextView resultName, resultDate, resultDescription, resultModel, resClassification;
        public ResultApdaterVH(@NonNull View itemView) {
            super(itemView);
            resultName = itemView.findViewById(R.id.resClassificationName);
            resultDate = itemView.findViewById(R.id.resClassificationDate);
            resultDescription = itemView.findViewById(R.id.resClassificationDescription);
            resultModel = itemView.findViewById(R.id.resModel);
            resClassification = itemView.findViewById(R.id.resClassification);
        }
    }
}
