package com.uscopex.main;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.SearchView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gareth.R;
import com.uscopex.adapters.ICMResultsAdapter;
import com.uscopex.responses.ICMResultSetResponse;

import java.util.List;

import com.uscopex.api.Client;
import com.uscopex.models.ICMResultSet;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import com.uscopex.storage.UserManager;

public class ICMCompletedProcessesActivity extends AppCompatActivity  {

    private int userId;
    RecyclerView recyclerView;
    ICMResultsAdapter icmResultsAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.icm_completed_processes_layout);
        userId = UserManager.getInstance(this).getUserAccount().getLoginId();
        getCompletedProcessesInformation();
        recyclerView = findViewById(R.id.resultsRecyclerViewICM);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new DividerItemDecoration(this,DividerItemDecoration.VERTICAL));
        icmResultsAdapter = new ICMResultsAdapter();
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("ICM Results");
    }


    private void getCompletedProcessesInformation() {
        Call<ICMResultSetResponse> call = Client.getClientInstance().getRestAPI().icmResults(userId);
        call.enqueue(new Callback<ICMResultSetResponse>() {
            @Override
            public void onResponse(Call<ICMResultSetResponse> call, Response<ICMResultSetResponse> response) {
                List<ICMResultSet> resultSetList = response.body().getResults();
                icmResultsAdapter.setResults(resultSetList);
                recyclerView.setAdapter(icmResultsAdapter);
            }
            @Override
            public void onFailure(Call<ICMResultSetResponse> call, Throwable t) {
                Toast.makeText(ICMCompletedProcessesActivity.this,"Error connecting to server",Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.results_search_menu,menu);
        MenuItem search = menu.findItem(R.id.resultSearch);
        SearchView searchView = (SearchView) search.getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                icmResultsAdapter.getFilter().filter(s);
                return false;
            }
        });
        return true;
    }
}

