package com.uscopex.main;

import android.app.DownloadManager;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import com.gareth.R;
import com.github.chrisbanes.photoview.PhotoView;
import com.squareup.picasso.Picasso;

import com.uscopex.storage.UserManager;

public class BatchResultActivity extends AppCompatActivity  {

    private static final String BASE_URL = "http://gjohnston32.lampt.eeecs.qub.ac.uk/uscopex_web/iam_processes/";
    private String analysisTitle;
    private  String imageDir;
    private  String resultDir;
    private boolean providesResults;
    private boolean providesImage;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.result_batch_activity_layout);
        Intent resultIntent = getIntent();
        LinearLayout layout = findViewById(R.id.relativeBatch);
        layout.setFocusable(false);
        String[] imagePaths= resultIntent.getStringArrayExtra("image_paths");
        String[] resultPaths= resultIntent.getStringArrayExtra("result_paths");
        String directoryName = resultIntent.getStringExtra("directory_name");
        int userId = UserManager.getInstance(this).getUserAccount().getLoginId();
        imageDir = "processed_images/_"+userId+"/batch_analysis/"+directoryName+"/";
        resultDir = "processed_results/_"+userId+"batch_analysis/"+directoryName+"/";
        analysisTitle = resultIntent.getStringExtra("analysis_name");

        providesImage = resultIntent.getBooleanExtra("provides_image",false);
        providesResults = resultIntent.getBooleanExtra("provides_results",false);
        TextView analysisNameText = findViewById(R.id.analysisNameResult);
        analysisNameText.setText(analysisTitle);

        for (int i = 0; i <= imagePaths.length -1; i++) {
            int finalI = i;
            TextView textView = new TextView(this);
            textView.setTextColor(Color.WHITE);
            textView.setText(imagePaths[i]);
            textView.setPadding(20,0,0,20);
            layout.addView(textView);
            PhotoView image = new PhotoView (BatchResultActivity.this);

            image.setAdjustViewBounds(true);
            if(providesImage) {
                Picasso.with(this)
                        .load(BASE_URL + imageDir + imagePaths[i])
                        .into(image);
            }
            layout.addView(image);
            LinearLayout.LayoutParams imageParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            imageParams.height = 100;
            imageParams.width = 500;
            imageParams.bottomMargin=30;
            imageParams.topMargin=30;

            LinearLayout.LayoutParams resultsParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            resultsParams.height = 100;
            resultsParams.width = 500;
            resultsParams.bottomMargin =100;

            if(providesResults) {
                Button downloadResults = new Button(this);
                downloadResults.setText("Download results");
                downloadResults.setBackgroundColor(Color.rgb(168, 14, 14));
                downloadResults.setTextColor(Color.WHITE);
                downloadResults.setPadding(10, 20, 10, 20);
                downloadResults.setLayoutParams(resultsParams);
                layout.addView(downloadResults);
                downloadResults.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        downloadResultFile(resultPaths[finalI], resultDir);
                    }
                });
            }
            if(providesImage) {
                Button downloadImage = new Button(this);
                downloadImage.setText("Download Image");
                downloadImage.setBackgroundColor(Color.rgb(71, 145, 91));
                downloadImage.setPadding(10, 20, 10, 20);
                downloadImage.setLayoutParams(imageParams);
                downloadImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        downloadResultFile(imagePaths[finalI], imageDir);
                    }
                });
                layout.addView(downloadImage);
            }
        }
    }

    public void downloadResultFile(String resultPath,String directory) {
        runOnUiThread(() -> {
            DownloadManager downloadmanager = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
            Uri uri = Uri.parse(BASE_URL +directory+resultPath);
            DownloadManager.Request request = new DownloadManager.Request(uri);
            request.setTitle(analysisTitle+"_"+resultPath);
            request.setDescription("Downloading");
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
            request.setVisibleInDownloadsUi(false);
            request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, resultPath);
            downloadmanager.enqueue(request);
        });
    }
}