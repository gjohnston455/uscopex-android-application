package com.uscopex.main;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import com.uscopex.ImageModifier.ZoomFunctionality;
import com.gareth.R;
import com.uscopex.account.LoginActivity;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import com.uscopex.api.ClientICM;
import com.uscopex.api.ClientScalar;
import com.uscopex.responses.ClassificationResponse;
import com.uscopex.models.MLModel;
import com.uscopex.models.User;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import com.uscopex.storage.UserManager;
import static android.R.layout.simple_spinner_item;

public class IcmActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int REQUEST_IMAGE_CAPTURE = 1;
    static final int REQUEST_TAKE_PHOTO = 1;
    private static final int LAUNCH_SECOND_ACTIVITY = 2;
    private static final int LAUNCH_THIRD_ACTIVITY = 3;
    public static int defaultPosition;
    private TextView textView, textView2;
    private ArrayList<MLModel> accessModelsArray;
    private ArrayList<String> modelNames = new ArrayList<String>();
    private Spinner spinner;
    private int modelId;
    ImageView imageView;
    Button classifyBtn;
    String currentImagePath;
    private EditText classificationNameEditT, descriptionEditT;
    private TextView textViewResult, predictionView;
    User user;
    private MLModel spinnerModel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        modelNames.add("--- Select model  ---");
        setContentView(R.layout.icm_activity_layout);
        findViewById(R.id.zoomButton).setOnClickListener(this);
        classifyBtn = findViewById(R.id.classify);
        findViewById(R.id.btn_takePhoto).setOnClickListener(this);
        findViewById(R.id.classify).setOnClickListener(this);
        user = UserManager.getInstance(this).getUserAccount();
        spinner = findViewById(R.id.modelSpinner);
        imageView = findViewById(R.id.imageView2);
        classificationNameEditT = findViewById(R.id.classificationNameEditT);
        descriptionEditT = findViewById(R.id.descriptionEditT);
        textViewResult = findViewById(R.id.resultView);
        predictionView = findViewById(R.id.predictionView);
        predictionView.setVisibility(View.INVISIBLE);

        fetchModels(user);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ((TextView) parent.getChildAt(0)).setTextColor(Color.parseColor("#bd7d4b"));
                defaultPosition = position;

               /*
                 * The --Select Model -- element is the first index of the accessModelsArray
                 * Must ensure it is not selected, and then remove its index
                 * to get the correct id of the selected item in the array.
                 */

                if(defaultPosition>0) {
                    modelId = accessModelsArray.get(defaultPosition - 1).getID();

                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) { }}); }

    @Override
    protected void onStart() {
        super.onStart();

        if (!UserManager.getInstance(this).isUserLoggedIn()) {
            Intent intent = new Intent(this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }
    }

    private void fetchModels(User user) {
        Call<String> call = ClientScalar.getClientInstance().getRestAPI().model(user.getLoginId());
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        String jsonresponse = response.body();
                        displayModels(jsonresponse);
                    }
                }
            }
            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(IcmActivity.this,"Error connecting to server",Toast.LENGTH_SHORT).show();
            }});
    }

    private void displayModels(String response) {

        try {
            JSONObject obj = new JSONObject(response);
            if (obj.optString("error").equals("false")) {

                    accessModelsArray = new ArrayList<>();
                    JSONArray dataArray = obj.getJSONArray("model");

                    for (int i = 0; i < dataArray.length(); i++) {
                        spinnerModel = new MLModel();
                        JSONObject dataobj = dataArray.getJSONObject(i);
                        spinnerModel.setID(dataobj.getInt("ID"));
                        spinnerModel.setName(dataobj.getString("Name"));
                        spinnerModel.setClassNames(dataobj.getString("ClassNames"));
                        accessModelsArray.add(spinnerModel);
                    }
                    for (int i = 0; i < accessModelsArray.size(); i++) {
                        modelNames.add(accessModelsArray.get(i).getName());
                    }
                    ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(IcmActivity.this, simple_spinner_item, modelNames);
                    spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinner.setAdapter(spinnerArrayAdapter);
                }
        } catch (JSONException e) { e.printStackTrace(); }
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.btn_takePhoto:
                cameraPermission();
                break;
            case R.id.classify:
                classificationRequest();
                break;
            case R.id.zoomButton:
                if (currentImagePath != null) {
                    Intent zoomIntent = new Intent(this, ZoomFunctionality.class);
                    zoomIntent.putExtra("image_path", currentImagePath);
                    startActivity(zoomIntent);
                }
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + view.getId());
        }
    }

    private void cameraPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, 1);
        } else {
            dispatchTakePictureIntent();
        }
    }


    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            File photoFile = null;
            try {
                photoFile = getImageFile();
            } catch (IOException ex) { }
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "com.g.mydomain.fileprovider", photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, 1);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1 && resultCode == RESULT_OK) {
            Bitmap bitmap = BitmapFactory.decodeFile(currentImagePath);
            imageView.setImageBitmap(rotateImage(bitmap, 90));
        }
        if (requestCode == LAUNCH_THIRD_ACTIVITY) { }
    }

    private File getImageFile() throws IOException {
        String time = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imgIdentity = "jpg " + time + " ";
        File storageDirectory = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(imgIdentity, ".jpg", storageDirectory);
        currentImagePath = image.getAbsolutePath();
        return image;
    }

    private static Bitmap rotateImage(Bitmap img, int degree) {
        Matrix matrix = new Matrix();
        matrix.postRotate(degree);
        Bitmap rotatedImg = Bitmap.createBitmap(img, 0, 0, img.getWidth(), img.getHeight(), matrix, true);
        img.recycle();
        return rotatedImg;
    }

    private void classificationRequest() {
        if (currentImagePath == null) {
            classifyBtn.setError("Image is required");
            return;
        }

        if(spinner.getSelectedItem().toString().equals("--- Select model  ---")){
            Toast.makeText(this,"Please select a model",Toast.LENGTH_LONG);
            return;
        }

        String classificationName = classificationNameEditT.getText().toString().trim();
        String description = descriptionEditT.getText().toString().trim();
        String classificationModel = spinner.getSelectedItem().toString();

        if (classificationName.isEmpty()) {
            classificationNameEditT.setError("You must enter an analysis name");
            classificationNameEditT.requestFocus();
            return;
        }
        if (description.isEmpty()) {
            descriptionEditT.setError("Description required");
            descriptionEditT.requestFocus();
            return;
        }
        Toast.makeText(this, "Classifying", Toast.LENGTH_SHORT).show();
        textViewResult.setText(" ");

        File file = new File(currentImagePath);
        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        MultipartBody.Part body = MultipartBody.Part.createFormData("imagefile", file.getName(), requestFile);

        RequestBody classificationNameRequest = RequestBody.create(MediaType.parse("multipart/form-data"), classificationName);
        RequestBody userIDRequest = RequestBody.create(MediaType.parse("multipart/form-data"), String.valueOf(user.getLoginId()));
        RequestBody descriptionRequest = RequestBody.create(MediaType.parse("multipart/form-data"), description);
        RequestBody modelNameRequest = RequestBody.create(MediaType.parse("multipart/form-data"), classificationModel);
        RequestBody mLModelId = RequestBody.create(MediaType.parse("multipart/form-data"), String.valueOf(modelId));

        Log.e("Model ID", String.valueOf(modelId));
        Call<ClassificationResponse> call = ClientICM.getClientInstance()
                .getRestAPI()
                .classification(userIDRequest, modelNameRequest, body, classificationNameRequest, descriptionRequest,mLModelId);

        call.enqueue(new Callback<ClassificationResponse>() {
            @Override
            public void onResponse(Call<ClassificationResponse> call, Response<ClassificationResponse> response) {
                predictionView.setVisibility(View.VISIBLE);

                if (!response.body().isError()) {
                    ToneGenerator toneGen1 = new ToneGenerator(AudioManager.STREAM_MUSIC, 100);
                    textViewResult.setText(classificationName(response.body().getMessage()));
                    toneGen1.startTone(ToneGenerator.TONE_CDMA_PIP, 100);
                    descriptionEditT.setText("");
                    classificationNameEditT.setText("");
                }
            }

            @Override
            public void onFailure(Call<ClassificationResponse> call, Throwable t) {
                Toast.makeText(IcmActivity.this,"Error connecting to server",Toast.LENGTH_SHORT).show();
            }
        });
    }

    public String classificationName(String prediction) {
        String classNames = accessModelsArray.get(spinner.getSelectedItemPosition() - 1).getClassNames();
        String[] classArray = classNames.split(",");
        return classArray[new Integer(prediction)];
    }
}
