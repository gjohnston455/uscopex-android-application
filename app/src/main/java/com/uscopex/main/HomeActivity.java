package com.uscopex.main;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;

import com.gareth.R;
import com.uscopex.account.LoginActivity;
import com.google.android.material.navigation.NavigationView;

import com.uscopex.storage.UserManager;

public class HomeActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_activity_layout);
        findViewById(R.id.analysisButton).setOnClickListener(this);
        findViewById(R.id.batchAnalysis).setOnClickListener(this);
        findViewById(R.id.classificationButton).setOnClickListener(this);
        findViewById(R.id.completedSingularProcesses).setOnClickListener(this);
        findViewById(R.id.icmResultsHome).setOnClickListener(this);
        findViewById(R.id.resultBatchHome).setOnClickListener(this);
        NavigationView navigationView = (NavigationView) findViewById(R.id.navHome);
        navigationView.bringToFront();


        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {

                        switch (menuItem.getItemId()) {
                            case R.id.nav_logout:
                                logOut();
                                break;
                            case R.id.discover_pipeline:
                                discoverPipelines();
                                break;
                            case R.id.nav_account:
                                accountSettings();
                                break;
                            case R.id.discover_model:
                                discoverModels();
                                break;
                            default:
                                return true;
                        }
                        return true;
                    }
                });
    }

    private void discoverModels() {
        Intent intent = new Intent(this, DiscoverMLModelsActivity.class);
        startActivity(intent);
    }

    private void accountSettings() {
        Intent intent = new Intent(this, AccountSettingsActivity.class);
        startActivity(intent);
    }

    private void discoverPipelines() {
        Intent intent = new Intent(this, DiscoverPipelinesActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (!UserManager.getInstance(this).isUserLoggedIn()) {
            Intent intent = new Intent(this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.analysisButton:
                Intent analysisIntent = new Intent(this, IamActivity.class);
                startActivity(analysisIntent);
                break;
            case R.id.classificationButton:
                Intent classificationIntent = new Intent(this, IcmActivity.class);
                startActivity(classificationIntent);
                break;
            case R.id.batchAnalysis:
                Intent batchIntent = new Intent(this, BatchIAMActivity.class);
                startActivity(batchIntent);
                break;
            case R.id.completedSingularProcesses:
                Intent resIntent = new Intent(this, IAMCompletedProcessesActivity.class);
                resIntent.putExtra("is_batch", 0);
                startActivity(resIntent);
                break;
            case R.id.resultBatchHome:
                Intent batchResIntent = new Intent(this, IAMCompletedProcessesActivity.class);
                batchResIntent.putExtra("is_batch", 1);
                startActivity(batchResIntent);
                break;
            case R.id.icmResultsHome:
                Intent resIntent2 = new Intent(this, ICMCompletedProcessesActivity.class);
                startActivity(resIntent2);
                break;
            default:
                break;
        }
    }

    public void logOut() {
        UserManager.getInstance(HomeActivity.this).logOut();
        Intent intent = new Intent(this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }
}
