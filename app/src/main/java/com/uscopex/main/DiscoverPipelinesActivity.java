package com.uscopex.main;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.SearchView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gareth.R;
import com.uscopex.adapters.DiscoverPipelinesAdapter;
import com.uscopex.models.DiscoverPipelinesModel;
import com.uscopex.responses.DiscoverPipelinesResponse;

import java.util.List;

import com.uscopex.api.Client;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import com.uscopex.storage.UserManager;

public class DiscoverPipelinesActivity extends AppCompatActivity implements DiscoverPipelinesAdapter.ClickedButtonListener {

    private int userId;
    RecyclerView recyclerView;
    DiscoverPipelinesAdapter discoverPipelinesAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.discover_pipelines_activity_layout);
        userId = UserManager.getInstance(this).getUserAccount().getLoginId();

        getAllPipelines();

        recyclerView = findViewById(R.id.resultsRecyclerViewPipelines);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new DividerItemDecoration(this,DividerItemDecoration.VERTICAL));
        discoverPipelinesAdapter = new DiscoverPipelinesAdapter(this::clickedResult);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Discover Analysis Pipelines");
    }


    private void getAllPipelines() {

        Call<DiscoverPipelinesResponse> call = Client.getClientInstance().getRestAPI().discoverPipelines(userId);
        call.enqueue(new Callback<DiscoverPipelinesResponse>() {
            @Override
            public void onResponse(Call<DiscoverPipelinesResponse> call, Response<DiscoverPipelinesResponse> response) {
                Log.i("Response ", String.valueOf(response.body().getResults().get(1).getiD()));
                List<DiscoverPipelinesModel> resultSetList = response.body().getResults();
                discoverPipelinesAdapter.setResults(resultSetList);
                recyclerView.setAdapter(discoverPipelinesAdapter);
            }
            @Override
            public void onFailure(Call<DiscoverPipelinesResponse> call, Throwable t) {
                Toast.makeText(DiscoverPipelinesActivity.this,"Error connecting to server",Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.results_search_menu,menu);
        MenuItem search = menu.findItem(R.id.resultSearch);
        SearchView searchView = (SearchView) search.getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                discoverPipelinesAdapter.getFilter().filter(s);
                return false;
            }
        });
        return true;
    }

    @Override
    public void clickedResult(DiscoverPipelinesModel resultSetResponse) {
        goToVersionsActivity(resultSetResponse.getiD(),resultSetResponse.getName());
    }

    private void goToVersionsActivity(int clickedPipelineId,String name){
        Intent versionsIntent = new Intent(this,DiscoverPipelineVersionsActivity.class);
        versionsIntent.putExtra("pipeline_id",clickedPipelineId);
        versionsIntent.putExtra("pipeline_name",name);

        startActivity(versionsIntent);
    }
}



