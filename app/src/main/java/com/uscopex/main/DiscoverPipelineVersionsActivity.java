package com.uscopex.main;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gareth.R;
import com.uscopex.adapters.DiscoverPipelineVersionsAdapter;
import com.uscopex.responses.DiscoverPipelineVersionResponse;

import com.uscopex.models.DiscoverPipelinesVersionsModel;

import java.util.List;

import com.uscopex.api.Client;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DiscoverPipelineVersionsActivity extends AppCompatActivity implements DiscoverPipelineVersionsAdapter.ClickedButtonListener {

    private int pipelineId;
    private String pipelineName;
    RecyclerView recyclerView;
    DiscoverPipelineVersionsAdapter discoverPipelineVersionsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent resultIntent = getIntent();
        pipelineId = resultIntent.getIntExtra("pipeline_id",-1);
        pipelineName =resultIntent.getStringExtra("pipeline_name");
        setContentView(R.layout.discover_pipeline_versions_activity_layout);
        getAllPipelineVersions();
        recyclerView = findViewById(R.id.resultsRecyclerViewPipelineVersions);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new DividerItemDecoration(this,DividerItemDecoration.VERTICAL));
        discoverPipelineVersionsAdapter = new DiscoverPipelineVersionsAdapter(this::clickedResult);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(pipelineName );
    }

    private void getAllPipelineVersions() {

        Call<DiscoverPipelineVersionResponse> call = Client.getClientInstance().getRestAPI().discoverPipelineVersions(pipelineId);
        call.enqueue(new Callback<DiscoverPipelineVersionResponse>() {
            @Override
            public void onResponse(Call<DiscoverPipelineVersionResponse> call, Response<DiscoverPipelineVersionResponse> response) {
                List<DiscoverPipelinesVersionsModel> resultSetList = response.body().getResults();

                discoverPipelineVersionsAdapter.setResults(resultSetList);
                recyclerView.setAdapter(discoverPipelineVersionsAdapter);
            }
            @Override
            public void onFailure(Call<DiscoverPipelineVersionResponse> call, Throwable t) {
                Toast.makeText(DiscoverPipelineVersionsActivity.this,"Error connecting to server",Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void clickedResult(DiscoverPipelinesVersionsModel resultSetResponse) {
    }
}



