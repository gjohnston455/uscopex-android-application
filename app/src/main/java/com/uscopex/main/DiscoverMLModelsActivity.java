package com.uscopex.main;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.SearchView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gareth.R;
import com.uscopex.adapters.DiscoverMLModelsAdapter;
import com.uscopex.responses.DiscoverMLModelsResponse;

import java.util.List;

import com.uscopex.api.Client;
import com.uscopex.models.DiscoverMLModelsModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import com.uscopex.storage.UserManager;

public class DiscoverMLModelsActivity extends AppCompatActivity  {

    private int userId;
    RecyclerView recyclerView;
    DiscoverMLModelsAdapter discoverMLModelsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.discover_ml_models_activity_layout);
        userId = UserManager.getInstance(this).getUserAccount().getLoginId();
        getAllModels();
        recyclerView = findViewById(R.id.resultsRecyclerViewMLModels);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new DividerItemDecoration(this,DividerItemDecoration.VERTICAL));
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Discover ML Models");
        discoverMLModelsAdapter = new DiscoverMLModelsAdapter();
    }

    private void getAllModels() {

        Call<DiscoverMLModelsResponse> call = Client.getClientInstance().getRestAPI().discoverModels(userId);
        call.enqueue(new Callback<DiscoverMLModelsResponse>() {

            @Override
            public void onResponse(Call<DiscoverMLModelsResponse> call, Response<DiscoverMLModelsResponse> response) {
                List<DiscoverMLModelsModel> resultSetList = response.body().getResults();
                discoverMLModelsAdapter.setResults(resultSetList);
                recyclerView.setAdapter(discoverMLModelsAdapter);
            }
            @Override
            public void onFailure(Call<DiscoverMLModelsResponse> call, Throwable t) {
                Toast.makeText(DiscoverMLModelsActivity.this,"Error connecting to server",Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.results_search_menu,menu);
        MenuItem search = menu.findItem(R.id.resultSearch);
        SearchView searchView = (SearchView) search.getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                discoverMLModelsAdapter.getFilter().filter(s);
                return false;
            }
        });
        return true;
    }
}



