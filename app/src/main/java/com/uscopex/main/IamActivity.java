package com.uscopex.main;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import com.gareth.R;
import com.uscopex.account.LoginActivity;
import com.uscopex.ImageModifier.SetROIActivity;
import com.uscopex.ImageModifier.SetScaleActivity;
import com.uscopex.ImageModifier.ZoomFunctionality;
import com.uscopex.utils.PathUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import static android.R.layout.simple_spinner_item;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.uscopex.api.Client;
import com.uscopex.api.ClientIAM;
import com.uscopex.api.ClientScalar;
import com.uscopex.responses.AnalysisRequestResponse;
import com.uscopex.responses.AnalysisResponse;
import com.uscopex.models.Pipeline;
import com.uscopex.models.PipelineProperties;
import com.uscopex.models.ROI;
import com.uscopex.models.Scale;
import com.uscopex.models.User;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import com.uscopex.storage.UserManager;

public class IamActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int REQUEST_IMAGE_CAPTURE = 1;
    static final int REQUEST_TAKE_PHOTO = 1;
    private static final int LAUNCH_SECOND_ACTIVITY = 2;
    private static final int LAUNCH_THIRD_ACTIVITY = 3;
    public static int defaultPosition;
    private ArrayList<PipelineProperties> pipelinePropertiesArray;
    private PipelineProperties pipelineProperties;
    private ArrayList<Pipeline> accessPipelinesArray;
    private ArrayList<String> pipelineNames = new ArrayList<String>();
    private Spinner spinner;
    private Spinner versionSpinner;
    private  MultipartBody.Part imageToSend;
    private  Bitmap bitmap;
    private  String analysisName;
    private int  userId;
    ImageView imageView, zoomButton;
    Button analysisBtn,loadImage;
    ImageButton cameraBtn;
    Switch global;
    String currentImagePath;
    private EditText analysisNameEditT, descriptionEditT, scaleEditText, scaleKnownEditText;
    ROI roi;
    Scale scale;
    private boolean roiRequired;
    private boolean scalesRequired;
    boolean roiSet;
    boolean scaleSet;
    private Pipeline spinnerModel;
    boolean analysisPipelineVersionSelected;
    int scaleCalculatedDistance;
    String pipelineVersion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pipelineNames.add("--- Select pipeline  ---");
        setContentView(R.layout.iam_activity_layout);
        cameraBtn = findViewById(R.id.btn_takePhoto);
        findViewById(R.id.zoomButton).setOnClickListener(this);
        analysisBtn = findViewById(R.id.analysis);
        findViewById(R.id.btn_takePhoto).setOnClickListener(this);
        findViewById(R.id.analysis).setOnClickListener(this);
        findViewById(R.id.setRoi).setOnClickListener(this);
        findViewById(R.id.setRoi).setVisibility(View.INVISIBLE);
        findViewById(R.id.setScale).setOnClickListener(this);
        findViewById(R.id.setScale).setVisibility(View.INVISIBLE);
        User user = UserManager.getInstance(this).getUserAccount();
        global = findViewById(R.id.globalSwitch);
        spinner = findViewById(R.id.analysisSpinner);
        versionSpinner = findViewById(R.id.analysisSpinner2);
        versionSpinner.setVisibility(View.INVISIBLE);
        imageView = findViewById(R.id.imageView2);
        analysisNameEditT = findViewById(R.id.analysisNameEditT);
        descriptionEditT = findViewById(R.id.descriptionEditT);
        scaleEditText = findViewById(R.id.scaleUnitEditT);
        scaleKnownEditText = findViewById(R.id.scaleKnownEditT2);
        scaleKnownEditText.setInputType(InputType.TYPE_CLASS_NUMBER);
        scaleKnownEditText.setVisibility(View.GONE);
        scaleEditText.setVisibility(View.GONE);
        global.setVisibility(View.INVISIBLE);

        fetchPipelines(user);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                findViewById(R.id.setRoi).setVisibility(View.INVISIBLE);
                findViewById(R.id.setScale).setVisibility(View.INVISIBLE);
                ((TextView) parent.getChildAt(0)).setTextColor(Color.parseColor("#bd7d4b"));
                defaultPosition = position;

                if (defaultPosition > 0) {
                    ((TextView) parent.getChildAt(0)).setTextColor(Color.parseColor("#8fa6a0"));
                    getPipelineVersions(accessPipelinesArray.get(defaultPosition - 1).getID());
                    if (accessPipelinesArray.get(defaultPosition - 1).getVersions() == 1) {
                        versionSpinner.setClickable(false);
                        versionSpinner.setBackground(ContextCompat.getDrawable(IamActivity.this, R.drawable.spinnerblank));
                    } else {
                        versionSpinner.setClickable(true);
                        versionSpinner.setBackground(ContextCompat.getDrawable(IamActivity.this, R.drawable.spinner));
                    }
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        versionSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                ((TextView) adapterView.getChildAt(0)).setTextColor(Color.parseColor("#bd7d4b"));
                defaultPosition = i;
                versionSpinner.setVisibility(View.VISIBLE);
                roiRequired = pipelinePropertiesArray.get(defaultPosition).isRoi();
                scalesRequired= pipelinePropertiesArray.get(defaultPosition).isScale();
                modifierButtonVisibility();
                analysisPipelineVersionSelected = true;
                pipelineVersion = pipelinePropertiesArray.get(i).getVersionNumber();
                pipelineProperties = pipelinePropertiesArray.get(i);

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) { }});

        cameraBtn.setOnLongClickListener(v -> {
            loadPhoto();
            return true;
        });
    }

    private void modifierButtonVisibility() {
        if(scalesRequired) {
            findViewById(R.id.setScale).setVisibility(View.VISIBLE);
        }else{
            findViewById(R.id.setScale).setVisibility(View.INVISIBLE);
        }
        if(roiRequired) {
            findViewById(R.id.setRoi).setVisibility(View.VISIBLE);
        }else{
            findViewById(R.id.setRoi).setVisibility(View.INVISIBLE);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (!UserManager.getInstance(this).isUserLoggedIn()) {
            Intent intent = new Intent(this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 11);
        }
    }

    private void fetchPipelines(User user) {

        Call<String> call = ClientScalar.getClientInstance().getRestAPI().pipeline(user.getLoginId());
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        Log.i("onSuccess", response.body());
                        String jsonresponse = response.body();
                        displayPipelinesInSpinner(jsonresponse);
                    }
                }
            }
            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(IamActivity.this,"Error connecting to server",Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void displayPipelinesInSpinner(String response) {

        try {
            JSONObject jsonResponseObj = new JSONObject(response);

            if (jsonResponseObj.optString("error").equals("false")) {

                if (jsonResponseObj.optString("message").equals("Versions")) {
                    pipelinePropertiesArray = new ArrayList<>();
                    JSONArray dataArray = jsonResponseObj.getJSONArray("pipeline_versions");
                    Log.i("lenght", String.valueOf(dataArray.length()));
                    for (int i = dataArray.length()-1; i >=0; i--) {
                        JSONObject dataobj = dataArray.getJSONObject(i);
                        PipelineProperties pipelineVersion = new PipelineProperties();
                        pipelineVersion.setVersionNumber(dataobj.getString("Version"));
                        pipelineVersion.setVersionId(dataobj.getInt("ID"));
                        pipelineVersion.setRoi((dataobj.getInt("Requires_Roi") > 0)? true : false);
                        pipelineVersion.setScale((dataobj.getInt("Requires_Scale")>0) ? true : false);
                        pipelineVersion.setProducingImage((dataobj.getInt("Image")>0) ? true : false);
                        pipelineVersion.setProducingResults((dataobj.getInt("Result_Set")>0) ? true : false);
                        pipelinePropertiesArray.add(pipelineVersion);
                    }

                    ArrayList<String> versionNumbers = new ArrayList<>();
                    for(int i = 0; i< pipelinePropertiesArray.size(); i++){
                        versionNumbers.add(((pipelinePropertiesArray.get(i).getVersionNumber())));
                    }

                    ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(IamActivity.this, simple_spinner_item, versionNumbers);
                    spinnerArrayAdapter.setDropDownViewResource(simple_spinner_item);
                    versionSpinner.setAdapter(spinnerArrayAdapter);
                } else {
                    accessPipelinesArray = new ArrayList<Pipeline>();
                    JSONArray dataArray = jsonResponseObj.getJSONArray("pipeline");
                    for (int i = 0; i < dataArray.length(); i++) {
                        spinnerModel = new Pipeline();
                        JSONObject dataobj = dataArray.getJSONObject(i);
                        spinnerModel.setName(dataobj.getString("Name"));
                        spinnerModel.setID(dataobj.getInt("ID"));
                        spinnerModel.setVersions(dataobj.getInt("Versions"));
                        accessPipelinesArray.add(spinnerModel);
                    }
                    for (int i = 0; i < accessPipelinesArray.size(); i++) {
                        pipelineNames.add(accessPipelinesArray.get(i).getName());
                    }
                    ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(IamActivity.this, simple_spinner_item, pipelineNames);
                    spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
                    spinner.setAdapter(spinnerArrayAdapter);
                }
            }
        } catch (JSONException e) {
        }
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.btn_takePhoto:
                cameraPermission();
                break;
            case R.id.analysis:
                if (currentImagePath != null) {
                    validateProcessName();
                }else{
                    Toast.makeText(this,"Please capture image",Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.setRoi:
                if (currentImagePath != null) {
                    Intent roiAction = new Intent(this, SetROIActivity.class);
                    roiAction.putExtra("image_path", currentImagePath);
                    startActivityForResult(roiAction, LAUNCH_SECOND_ACTIVITY);
                }
                break;
            case R.id.setScale:
                if (currentImagePath != null) {
                    Intent setScaleAction = new Intent(this, SetScaleActivity.class);
                    setScaleAction.putExtra("image_path", currentImagePath);
                    startActivityForResult(setScaleAction, LAUNCH_THIRD_ACTIVITY);
                }
                break;
            case R.id.zoomButton:
                if (currentImagePath != null) {
                    Intent zoomIntent = new Intent(this, ZoomFunctionality.class);
                    zoomIntent.putExtra("image_path", currentImagePath);
                    startActivity(zoomIntent);
                }
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + view.getId());
        }
    }

    private void loadPhoto() {
        Intent pickPhoto = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(pickPhoto , 10);
    }

    private void cameraPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, 1);
        } else {
            dispatchTakePictureIntent();
        }
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            File photoFile = null;
            try {
                photoFile = getImageFile();
            } catch (IOException ex) {
            }
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "com.g.mydomain.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, 1);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e("getting ", String.valueOf(requestCode));
        if (requestCode == 1 && resultCode == RESULT_OK) {
            bitmap = BitmapFactory.decodeFile(currentImagePath);
            imageView.setImageBitmap(rotateImage(bitmap, 90));
        }
        if(requestCode ==10 && resultCode == RESULT_OK) {
            Uri selectedImage = data.getData();
            imageView.setImageURI(selectedImage);
            try {
                currentImagePath = PathUtil.getPath(this, selectedImage);
                File file = new File(currentImagePath);
                RequestBody requestBody = RequestBody.create(MediaType.parse("*/*"), file);
                imageToSend = MultipartBody.Part.createFormData("img", file.getName(), requestBody);
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
        }
        if (requestCode == 2) {
            if (data.getBooleanExtra("roiSet", roiSet)) {
                int[] roiArray = data.getIntArrayExtra("roi");
                Toast toast = Toast.makeText(this, "ROI set", Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER, 0, 6);
                toast.show();
                roiSet = true;
                roi = new ROI(roiArray[0], roiArray[1], roiArray[2], roiArray[3]);
                Log.i("roi in main", roi.toString());
            } else {
                Toast toast = Toast.makeText(this, "ROI not set", Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER, 0, 6);
                toast.show();

            }
        }
        if (requestCode == LAUNCH_THIRD_ACTIVITY) {

            if (data.getBooleanExtra("scaleSet", scaleSet)) {
                scaleEditText.setVisibility(View.VISIBLE);
                scaleKnownEditText.setVisibility(View.VISIBLE);
                global.setVisibility(View.VISIBLE);
                scaleCalculatedDistance = data.getIntExtra("distance", 1);
                Toast toast = Toast.makeText(this, "Add units ", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 6);
                toast.show();
                scaleSet = true;
            } else {
                Toast toast = Toast.makeText(this, "Scale not set", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 6);
                toast.show();
            }
        }
    }

    private File getImageFile() throws IOException {
        String time = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imgIdentity = "jpg " + time + " ";
        File storageDirectory = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(imgIdentity, ".jpg", storageDirectory);
        RequestBody requestBody = RequestBody.create(MediaType.parse("*/*"), image);
        imageToSend = MultipartBody.Part.createFormData("img", image.getName(), requestBody);
        currentImagePath = image.getAbsolutePath();
        return image;
    }

    private static Bitmap rotateImage(Bitmap img, int degree) {
        Matrix matrix = new Matrix();
        matrix.postRotate(degree);
        Bitmap rotatedImg = Bitmap.createBitmap(img, 0, 0, img.getWidth(), img.getHeight(), matrix, true);
        img.recycle();
        return rotatedImg;
    }

    private void validateProcessName(){

        analysisName = analysisNameEditT.getText().toString().trim();

        if (analysisName.isEmpty()) {
            analysisNameEditT.setError("You must enter an analysis name");
            analysisNameEditT.requestFocus();
            return;
        }
        userId = UserManager.getInstance(this).getUserAccount().getLoginId();

        Call<AnalysisRequestResponse> insertCall = Client.getClientInstance()
                .getRestAPI()
                .analysisRequestValidation(analysisName,userId);
        insertCall.enqueue(new Callback<AnalysisRequestResponse>() {
            @Override
            public void onResponse(Call<AnalysisRequestResponse> call, Response<AnalysisRequestResponse> response) {

                if (!response.body().isError()) {
                    if (response.body().getMessage().equals("Available")) {
                        analysisRequest();
                    } else {
                        Toast.makeText(IamActivity.this, "You have already used this name", Toast.LENGTH_LONG).show();
                    }
                }
            }
            @Override
            public void onFailure(Call<AnalysisRequestResponse> call, Throwable t) {
                Toast.makeText(IamActivity.this, "Server is not responding",Toast.LENGTH_LONG).show();
            }
        });
    }

    private void analysisRequest() {

        if (currentImagePath == null) {
            analysisBtn.setError("Image is required");
            return;
        }
        String analysisPipeline = spinner.getSelectedItem().toString();

        if (analysisPipeline.equals("--- Select pipeline  ---")) {
            Toast.makeText(this, "You must select a valid pipeline", Toast.LENGTH_LONG);
            return;
        }
        String description = descriptionEditT.getText().toString().trim();


        if (scaleSet) {
            int knownDistance = 0;
            if(scaleKnownEditText.getText().length()!=0){
                knownDistance = new Integer(scaleKnownEditText.getText().toString().trim());
            }

            String units = scaleEditText.getText().toString().trim();
            if (units.length()==0l) {
                Toast.makeText(this,"Please specify a unit of measurement",Toast.LENGTH_LONG).show();
                return;
            }
            if (knownDistance == 0) {
                Toast.makeText(this,"Please specify the known distance as a number",Toast.LENGTH_LONG).show();
                return;
            }
            if(((Switch) findViewById(R.id.globalSwitch)).isChecked()){
                units += " global";
                Log.i("Scale", units);
            }
            scale = new Scale(knownDistance, units, scaleCalculatedDistance);
        }

        if (analysisName.isEmpty()) {
            analysisNameEditT.setError("You must enter an analysis name");
            analysisNameEditT.requestFocus();
            return;
        }
        if (description.isEmpty()) {
            descriptionEditT.setError("Description required");
            descriptionEditT.requestFocus();
            return;
        }

        if (roiRequired || scalesRequired) {
            if(scalesRequired && !scaleSet){
                Toast.makeText(this,"Scale is required",Toast.LENGTH_LONG).show();
                return;
            }
            if(roiRequired && !roiSet){
                Toast.makeText(this,"Roi is required",Toast.LENGTH_LONG).show();
                return;
            }
            ProgressDialog mDialog = new ProgressDialog(this);
            mDialog.setMessage("Processing...");
            mDialog.setCancelable(false);
            mDialog.show();
            pipelineProperties.setName(analysisPipeline);
            Log.i(roi.toString(),scale.toString());


            Call<AnalysisResponse> call = ClientIAM.getClientInstance().getRestAPI().
                    advancedAnalysis(
                            imageToSend,analysisName,description,userId,pipelineProperties,roi,scale
                    );
            call.enqueue(new Callback<AnalysisResponse>() {
                @Override
                public void onResponse(Call<AnalysisResponse> call, Response<AnalysisResponse> response) {
                    if (!response.body().isError()) {
                        mDialog.dismiss();
                        Toast.makeText(IamActivity.this, response.body().getMessage(), Toast.LENGTH_LONG).show();
                        new ToneGenerator(AudioManager.STREAM_MUSIC, 100).startTone(ToneGenerator.TONE_CDMA_PIP, 100);
                        showResults(response);

                    }
                }
                @Override
                public void onFailure(Call<AnalysisResponse> call, Throwable t) { }
            });
        } else {


            pipelineProperties.setName(analysisPipeline);

            Call<AnalysisResponse> call = ClientIAM.getClientInstance().getRestAPI().simpleAnalysis(
                    imageToSend,analysisName,
                    description,userId, pipelineProperties
                    );
            ProgressDialog mDialog = new ProgressDialog(this);
            mDialog.setMessage("Processing...");
            mDialog.setCancelable(false);
            mDialog.show();

            call.enqueue(new Callback<AnalysisResponse>() {
                @Override
                public void onResponse(Call<AnalysisResponse> call, Response<AnalysisResponse> response) {
                    if (!response.body().isError()) {
                        mDialog.dismiss();
                        Toast.makeText(IamActivity.this, response.body().getMessage(), Toast.LENGTH_LONG).show();
                        new ToneGenerator(AudioManager.STREAM_MUSIC, 100).startTone(ToneGenerator.TONE_CDMA_PIP, 100);
                        showResults(response);
                    }
                }
                @Override
                public void onFailure(Call<AnalysisResponse> call, Throwable t) {
                    Toast.makeText(IamActivity.this, t.getMessage(), Toast.LENGTH_LONG).show(); }
            });
        }
    }

        public void getPipelineVersions (int pipelineId){
            Call<String> call = ClientScalar.getClientInstance().getRestAPI().pipelineVersions(pipelineId);
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    if (response.isSuccessful()) {
                        if (response.body() != null) {
                            String jsonresponse = response.body();
                            displayPipelinesInSpinner(jsonresponse);
                        }
                    }
                }
                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    Toast.makeText(IamActivity.this,"Error connecting to server",Toast.LENGTH_SHORT).show();
                }
            });
        }

        private void showResults(Response<AnalysisResponse> response){
            Intent intent = new Intent(IamActivity.this,ResultActivity.class);
            intent.putExtra("imageURL",response.body().getImageName());
            intent.putExtra("results",response.body().getResultName());
            intent.putExtra("analysis_name",analysisName);
            startActivity(intent);
        }
    }
