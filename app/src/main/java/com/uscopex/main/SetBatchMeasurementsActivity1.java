package com.uscopex.main;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import com.uscopex.ImageModifier.SetROIActivity;
import com.uscopex.ImageModifier.SetScaleActivity;
import com.gareth.R;
import com.squareup.picasso.Picasso;
import java.io.File;
import java.util.ArrayList;
import com.uscopex.models.BatchAdvancedImageMeasurements;

public class SetBatchMeasurementsActivity1 extends AppCompatActivity implements View.OnClickListener {

    private static final int ROI_SET = 2;
    private static final int SCALE_SET = 15;
    private boolean roiSet;
    private int[] roiArray;
    private int scaleCalculatedDistance;
    private String units;
    private boolean scaleSet;
    private String currentImagePath;
    private int  requestCode;
    EditText scaleKnownEditText,scaleEditText;
    Switch global;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.batch_iam_set_measurements_activity_layout);
        findViewById(R.id.setRoiBatch2).setOnClickListener(this);
        findViewById(R.id.setScaleBatch2).setOnClickListener(this);
        findViewById(R.id.batchApply).setOnClickListener(this);

        scaleEditText = findViewById(R.id.scaleUnitEditTBatch2);
        scaleKnownEditText = findViewById(R.id.scaleKnownEditTBatch2);
        scaleKnownEditText.setInputType(InputType.TYPE_CLASS_NUMBER);
        global = findViewById(R.id.globalSwitchBatch2);

        Intent intent = getIntent();
        ImageView imageView = (ImageView) findViewById(R.id.batchImageView2);
        currentImagePath = intent.getStringExtra("image_path");

        Picasso.with(this).load(new File(currentImagePath)).into(imageView);
        requestCode = intent.getExtras().getInt("requestCode");

        if(requestCode == BatchIAMActivity.GET_MEASUREMENTS_ROI){
            findViewById(R.id.setScaleBatch2).setVisibility(View.INVISIBLE);
        }
        if(requestCode == BatchIAMActivity.GET_MEASUREMENTS_SCALE){
            findViewById(R.id.setRoiBatch2).setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.setRoiBatch2:
                if (currentImagePath != null) {
                    Intent roiAction = new Intent(this, SetROIActivity.class);
                    roiAction.putExtra("image_path", currentImagePath);
                    startActivityForResult(roiAction, ROI_SET);
                }
                break;
            case R.id.setScaleBatch2:
                if (currentImagePath != null) {
                    Intent setScaleAction = new Intent(this, SetScaleActivity.class);
                    setScaleAction.putExtra("image_path", currentImagePath);
                    startActivityForResult(setScaleAction, SCALE_SET);
                }
                break;
            case R.id.batchApply:
                apply();
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + view.getId());
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == ROI_SET) {
            if (data.getBooleanExtra("roiSet", roiSet)) {
                roiArray = data.getIntArrayExtra("roi");
                Toast toast = Toast.makeText(this, "ROI set", Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER, 0, 6);
                toast.show();
               roiSet = true;

            } else {
                Toast toast = Toast.makeText(this, "ROI not set", Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER, 0, 6);
                toast.show();
            }
        }
        if (requestCode == SCALE_SET) {
            if (data.getBooleanExtra("scaleSet", scaleSet)) {
                Toast toast = Toast.makeText(this, "Add units ", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 6);
                toast.show();
                scaleSet = true;
                scaleCalculatedDistance = data.getIntExtra("distance", 1);
                Log.e("Distance", Integer.toString(data.getIntExtra("distance", 1)));
                findViewById(R.id.scaleKnownEditTBatch2).setVisibility(View.VISIBLE);
                findViewById(R.id.scaleUnitEditTBatch2).setVisibility(View.VISIBLE);
                findViewById(R.id.globalSwitchBatch2).setVisibility(View.VISIBLE);
            } else {
                Toast toast = Toast.makeText(this, "Scale not set", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 6);
                toast.show();
            }
        }
    }


    private void apply() {

        int knownDistance = 0;
        if(requestCode!=BatchIAMActivity.GET_MEASUREMENTS_ROI) {
            if(!scaleSet){
                Toast.makeText(this,"Please set Scale",Toast.LENGTH_LONG).show();
                return;
            }
            if (scaleKnownEditText.getText().length() != 0) {
                knownDistance = new Integer(scaleKnownEditText.getText().toString().trim());
            }else{
                Toast.makeText(this,"Please set known distance",Toast.LENGTH_SHORT).show();
            }

             units = scaleEditText.getText().toString().trim();
            if (units.length() == 0) {
                Toast.makeText(this, "Please specify a unit of measurement", Toast.LENGTH_LONG).show();
                return;
            }
            if (knownDistance == 0) {
                Toast.makeText(this, "Please specify the known distance as a number", Toast.LENGTH_LONG).show();
                return;
            }
            if (((Switch) findViewById(R.id.globalSwitchBatch2)).isChecked()) {
                units += " global";
                Log.i("Scale", units);
            }
        }

        Intent returnResultsIntent = new Intent(this,BatchIAMActivity.class);
        if(requestCode == BatchIAMActivity.GET_MEASUREMENTS_ALL){
            if(!roiSet){
                Toast.makeText(this,"Please set ROI",Toast.LENGTH_LONG).show();
                return;
            }
            if(!scaleSet){
                Toast.makeText(this,"Please set Scale",Toast.LENGTH_LONG).show();
                return;
            }
            returnResultsIntent.putExtra("calculatedDistance",scaleCalculatedDistance);
            returnResultsIntent.putExtra("scaleSet",scaleSet);
            returnResultsIntent.putExtra("knownDistance",knownDistance);
            returnResultsIntent.putExtra("units",units);
            returnResultsIntent.putExtra("roiSet",roiSet);
            returnResultsIntent.putExtra("roi", roiArray);
            onActivityResult(BatchIAMActivity.GET_MEASUREMENTS_ALL, BatchIAMActivity.GET_MEASUREMENTS_ALL, returnResultsIntent);
            setResult(SetScaleActivity.RESULT_OK, returnResultsIntent);
            finish();
        }
        if(requestCode == BatchIAMActivity.GET_MEASUREMENTS_SCALE){
            returnResultsIntent.putExtra("calculatedDistance",scaleCalculatedDistance);
            returnResultsIntent.putExtra("scaleSet",scaleSet);
            returnResultsIntent.putExtra("knownDistance",knownDistance);
            returnResultsIntent.putExtra("units",units);
            onActivityResult(BatchIAMActivity.GET_MEASUREMENTS_SCALE, BatchIAMActivity.GET_MEASUREMENTS_SCALE, returnResultsIntent);
            setResult(SetScaleActivity.RESULT_OK, returnResultsIntent);
            finish();
        }
        if(requestCode == BatchIAMActivity.GET_MEASUREMENTS_ROI){
            if(!roiSet){
                Toast.makeText(this,"Please set ROI",Toast.LENGTH_LONG).show();
                return;
            }
            returnResultsIntent.putExtra("roiSet",roiSet);
            returnResultsIntent.putExtra("roi", roiArray);
            onActivityResult(BatchIAMActivity.GET_MEASUREMENTS_ROI, BatchIAMActivity.GET_MEASUREMENTS_ROI, returnResultsIntent);
            setResult(SetScaleActivity.RESULT_OK, returnResultsIntent);
            finish();
        }
    }
}




