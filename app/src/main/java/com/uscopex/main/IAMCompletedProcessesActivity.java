package com.uscopex.main;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.SearchView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.gareth.R;
import com.uscopex.adapters.IAMResultsAdapter;
import java.util.List;
import com.uscopex.api.Client;

import com.uscopex.responses.IAMResultPathsResponse;
import com.uscopex.models.IAMResultSet;
import com.uscopex.responses.IAMResultSetResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import com.uscopex.storage.UserManager;

public class IAMCompletedProcessesActivity extends AppCompatActivity implements IAMResultsAdapter.ClickedButtonListener {

    private int userId;
    private IAMResultSetResponse results;
    RecyclerView recyclerView;
    IAMResultsAdapter IAMResultsAdapter;
    boolean imageProvided;
    boolean resultProvided;
    int isBatch;
    String imagePath,resultPath;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.iam_completed_processes_layout);
        userId = UserManager.getInstance(this).getUserAccount().getLoginId();

        Intent resultIntent = getIntent();
        isBatch = resultIntent.getIntExtra("is_batch",-1);
        getCompletedProcessesInformation();
        imageProvided  = false;
        resultProvided = false;
        recyclerView = findViewById(R.id.resultsRecyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new DividerItemDecoration(this,DividerItemDecoration.VERTICAL));
        IAMResultsAdapter = new IAMResultsAdapter(this::clickedResult);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Results");
        if(isBatch==1)actionBar.setTitle("Batch Results");

    }


    private void getCompletedProcessesInformation() {

        Call<IAMResultSetResponse> call = Client.getClientInstance().getRestAPI().resultSets(userId, isBatch);
        call.enqueue(new Callback<IAMResultSetResponse>() {
            @Override
            public void onResponse(Call<IAMResultSetResponse> call, Response<IAMResultSetResponse> response) {
                List<IAMResultSet> resultSetList = response.body().getResults();
                IAMResultsAdapter.setResults(resultSetList);
                recyclerView.setAdapter(IAMResultsAdapter);
            }
            @Override
            public void onFailure(Call<IAMResultSetResponse> call, Throwable t) { }
        });
    }


    @Override
    public void clickedResult(IAMResultSet resultSetResponse) {
        if(resultSetResponse.getImagePath()!=null) {
            imageProvided = true;
            imagePath = resultSetResponse.getImagePath();
        }
        if(resultSetResponse.getResultPath()!=null){
            resultProvided = true;
            resultPath = resultSetResponse.getResultPath();
        }
        if(isBatch==1){
            getBatchResultPaths(resultSetResponse.getiD(),resultSetResponse.getName());
        }else {
            showResults(imagePath, resultPath, resultSetResponse.getName());
        }
    }

    private void showResults(String imagePath, String resultPath,String analysisName){
        Intent intent = new Intent(IAMCompletedProcessesActivity.this,ResultActivity.class);
        intent.putExtra("imageURL",imagePath);
        intent.putExtra("results",resultPath);
        intent.putExtra("analysis_name",analysisName);
        startActivity(intent);
    }
    private void showBatchResults(Response<IAMResultPathsResponse> response, String name){
        Intent intent = new Intent(this, BatchResultActivity.class);
        String[] imageNames = new String[response.body().getImagePaths().length];
        String[] resultsPaths = new String[response.body().getResultPaths().length];

        for (int i=0;i<response.body().getImagePaths().length;i++){
            imageNames[i] = response.body().getImagePaths()[i].getImagePath();
        }
        for (int i=0;i<response.body().getResultPaths().length;i++){
            resultsPaths[i] = response.body().getResultPaths()[i].getResultPath();
        }
        for(int i=0;i<imageNames.length;i++){
            Log.i("iMAGES : ",imageNames[i]);
        }
        if(imageNames.length>=1){
            imageProvided = true;
        }
        if(resultsPaths.length>=1){
            resultProvided=true;
        }
        intent.putExtra("image_paths", imageNames);
        intent.putExtra("result_paths", resultsPaths);
        intent.putExtra("directory_name",name);
        intent.putExtra("analysis_name",name);
        intent.putExtra("provides_image",imageProvided);
        intent.putExtra("results_provided",resultProvided);

        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.results_search_menu,menu);
        MenuItem search = menu.findItem(R.id.resultSearch);
        SearchView searchView = (SearchView) search.getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                IAMResultsAdapter.getFilter().filter(s);
                return false;
            }
        });
        return true;
    }

    private void getBatchResultPaths(int resultSetId,String name){

        Call<IAMResultPathsResponse> call = Client.getClientInstance().getRestAPI().batchResultPaths(resultSetId);
        call.enqueue(new Callback<IAMResultPathsResponse>() {
            @Override
            public void onResponse(Call<IAMResultPathsResponse> call, Response<IAMResultPathsResponse> response) {
                if(!response.body().isError()){
                    showBatchResults(response,name);
                }else {
                    Toast.makeText(IAMCompletedProcessesActivity.this,"Something went wrong, try again",Toast.LENGTH_LONG).show();
                }
            }
            @Override
            public void onFailure(Call<IAMResultPathsResponse> call, Throwable t) {
                Toast.makeText(IAMCompletedProcessesActivity.this,t.getMessage(),Toast.LENGTH_LONG).show();
            }
        });
    }
}

