package com.uscopex.main;

import android.app.DownloadManager;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.gareth.R;
import com.squareup.picasso.Picasso;

import com.uscopex.storage.UserManager;

public class ResultActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String BASE_URL = "http://gjohnston32.lampt.eeecs.qub.ac.uk/uscopex_web/iam_processes/processed_images/_";
    private static final String BASE_URL_RESULTS = "http://gjohnston32.lampt.eeecs.qub.ac.uk/uscopex_web/iam_processes/processed_results/_";
    private String resultName;
    private String analysisName;
    private int userId;
    private String imageUrl;
    ImageView imageView;
    TextView textViewResName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.result_activity_layout);
        findViewById(R.id.resultsDwn).setOnClickListener(this);
        findViewById(R.id.resultImage).setOnClickListener(this);
        imageView = findViewById(R.id.resultImage);
        textViewResName = findViewById(R.id.textViewResName);
        Intent resultIntent = getIntent();
        String imagePath =  resultIntent.getStringExtra("imageURL");
        resultName = resultIntent.getStringExtra("results");
        Log.e("file name",resultName);
        analysisName = resultIntent.getStringExtra("analysis_name");
        userId = UserManager.getInstance(this).getUserAccount().getLoginId();
        imageUrl = BASE_URL+userId+"/single_analysis/"+imagePath;

        textViewResName.setText(analysisName);
        Picasso.with(this)
                .load(imageUrl).rotate(90)
                .into(imageView);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.resultsDwn:
                downloadResultFile();
                break;
            case R.id.resultImage:
                downloadImage();
            default:
                break;
        }
    }

    public void downloadResultFile() {
        runOnUiThread(() -> {
            DownloadManager downloadmanager = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
            Uri uri = Uri.parse(BASE_URL_RESULTS+userId+"/single_analysis/"+resultName);
            Log.e("urls", BASE_URL_RESULTS+"_"+userId+"/single_analysis/"+resultName);
            DownloadManager.Request request = new DownloadManager.Request(uri);
            request.setTitle("My File");
            request.setDescription("Downloading");
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
            request.setVisibleInDownloadsUi(false);
            request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, "result.csv");
            downloadmanager.enqueue(request);
        });
    }

    public void downloadImage() {
        runOnUiThread(() -> {
            DownloadManager downloadmanager = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
            Uri uri = Uri.parse(imageUrl);
            DownloadManager.Request request = new DownloadManager.Request(uri);
            request.setTitle("My File");
            request.setDescription("Downloading");
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
            request.setVisibleInDownloadsUi(false);
            request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, "result.csv");
            downloadmanager.enqueue(request);
        });
    }
}
