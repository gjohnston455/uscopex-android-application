package com.uscopex.main;

import android.Manifest;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.content.ContextCompat;

import com.gareth.R;
import com.uscopex.account.LoginActivity;
import com.uscopex.utils.PathUtil;
import com.github.chrisbanes.photoview.PhotoView;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.concurrent.CountDownLatch;

import com.uscopex.api.Client;
import com.uscopex.api.ClientIAM;
import com.uscopex.api.ClientScalar;
import com.uscopex.responses.BatchRequestResponse;
import com.uscopex.responses.BatchResponse;

import com.uscopex.models.BatchAdvancedImageMeasurements;
import com.uscopex.models.BatchROI;
import com.uscopex.models.BatchScale;
import com.uscopex.models.Pipeline;
import com.uscopex.models.PipelineProperties;
import com.uscopex.models.ROI;
import com.uscopex.models.Scale;
import com.uscopex.models.User;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import com.uscopex.storage.UserManager;

import static android.R.layout.simple_spinner_item;

/*
 * Main activity for batch processing
 */
public class BatchIAMActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int LOAD_IMAGES = 1;
    private static final String NOTIFICATION_ID_STRING = "0";
    public static final int GET_MEASUREMENTS_ALL = 3;
    public static final int GET_MEASUREMENTS_ROI = 4;
    public static final int GET_MEASUREMENTS_SCALE = 5;
    private String analysisName;
    private String description;
    private String  analysisPipeline;
    private int pipelineVersionId;
    private int batchRequestId;
    private boolean[] measurementsSet;
    private ArrayList<String> paths;
    private ArrayList<BatchAdvancedImageMeasurements> advancedImages;
    private ArrayList<Uri> uriPaths;
    private ArrayList<PipelineProperties> pipelinePropertiesArrayList;
    private PipelineProperties pipelineProperties;
    public static int defaultPosition;
    private ArrayList<Pipeline> accessPipelinesArray;
    private ArrayList<String> pipelineNames = new ArrayList<String>();
    private Spinner spinner;
    private Spinner versionSpinner;
    private Pipeline spinnerModel;
    private int currentIndex;
    boolean analysisPipelineVersionSelected;
    EditText analysisNameEditT, descriptionEditT;
    CountDownLatch latch;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pipelineNames.add("--- Select pipeline  ---");
        createNotificationChannel();
        setContentView(R.layout.batch_iam_layout);
        findViewById(R.id.btnLoadBatch).setOnClickListener(this);
        findViewById(R.id.processBatch).setOnClickListener(this);
        analysisNameEditT = findViewById(R.id.analysisNameEditT);
        descriptionEditT = findViewById(R.id.descriptionEditT);
        spinner = findViewById(R.id.batchSpinner);
        versionSpinner = findViewById(R.id.batchSpinnerVersions);
        versionSpinner.setVisibility(View.INVISIBLE);
        User user = UserManager.getInstance(this).getUserAccount();


        fetchBatchReadyPipelines(user);
        /*
        Tracking the selection of pipelines in the spinner
         */
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ((TextView) parent.getChildAt(0)).setTextColor(Color.parseColor("#bd7d4b"));
                defaultPosition = position;
                if (defaultPosition > 0) {
                    ((TextView) parent.getChildAt(0)).setTextColor(Color.parseColor("#8fa6a0"));
                    getPipelineVersions(accessPipelinesArray.get(defaultPosition - 1).getID());
                    versionSpinner.setVisibility(View.VISIBLE);
                    if (accessPipelinesArray.get(defaultPosition - 1).getVersions() == 1) {
                        versionSpinner.setClickable(false);
                        versionSpinner.setBackground(ContextCompat.getDrawable(BatchIAMActivity.this, R.drawable.spinnerblank));
                    } else {
                        versionSpinner.setClickable(true);
                        versionSpinner.setBackground(ContextCompat.getDrawable(BatchIAMActivity.this, R.drawable.spinner));
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) { }
        });

        /*
        Tracking the selection of pipeline versions specific to the selected pipeline.
        Determines the specific properties of the version so to ensure the correct options are presented, com.uscopex.api calls are used
        and correct attributes are sent to the Analysis API.
        e.g. ROI/Scale ... advanced/basic .... outputs of the pipeline (i.e. image/results)
         */
        versionSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                ((TextView) adapterView.getChildAt(0)).setTextColor(Color.parseColor("#bd7d4b"));
                findViewById(R.id.measurementInfoText).setVisibility(View.INVISIBLE);
                defaultPosition = i;
                analysisPipelineVersionSelected = true;
                analysisPipelineVersionSelected = true;
                pipelineProperties =  pipelinePropertiesArrayList.get(i);
                if(pipelineProperties.isScale()|| pipelineProperties.isRoi()){
                    findViewById(R.id.measurementInfoText).setVisibility(View.VISIBLE);

                    /*
                      Keeping track of the setting of measurements for each image with a boolean array
                      Used to validate all measurements are gathered before payload is sent.
                     */
                    measurementsSet = new boolean[paths.size()];
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                findViewById(R.id.measurementInfoText).setVisibility(View.INVISIBLE);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (!UserManager.getInstance(this).isUserLoggedIn()) {
            Intent intent = new Intent(this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 11);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnLoadBatch:
                selectImages();
                break;
            case R.id.processBatch:
                batchRequestInsertCall();
                break;
            default:
                break;
        }
    }

    private void selectImages() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), LOAD_IMAGES);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == LOAD_IMAGES) {
            if (resultCode == BatchIAMActivity.RESULT_OK) {
                if (data.getClipData() != null) {
                    paths = new ArrayList<String>();
                    uriPaths = new ArrayList<Uri>();
                    advancedImages = new ArrayList<BatchAdvancedImageMeasurements>();
                    int count = data.getClipData().getItemCount();
                    latch = new CountDownLatch(count);
                    LinearLayout linearLayout = findViewById(R.id.linearLay);
                    for (int i = 0; i < count; i++) {
                        PhotoView photoView = new PhotoView(this);
                        photoView.setOnClickListener(this::onClick);
                        photoView.setPadding(0, 0, 10, 0);
                        photoView.setAdjustViewBounds(true);
                        Uri imageUri = data.getClipData().getItemAt(i).getUri();
                        try {
                            String path = PathUtil.getPath(this, imageUri);
                            uriPaths.add(imageUri);
                            paths.add(path);

                            /*
                             If an advanced process takes place these objects will be used to hold scale/roi information per image
                             */
                            advancedImages.add(new BatchAdvancedImageMeasurements());
                        } catch (URISyntaxException e) {
                            e.printStackTrace();
                        }
                        Picasso.with(this).load(imageUri).resize(600, 800).centerCrop().into(photoView);
                        linearLayout.addView(photoView);
                        int finalI = i;

                        /*
                          Each imageView will be clickable if the pipeline requires a scale or ROI.
                          If not, the on click will be blocked through validation in the measurementOperations method.
                          Passing image path and the increment value which will be used to ensure the correct object found in the
                          BatchAdvancedImageMeasurements array is populated
                         */
                        photoView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                measurementOperations(paths.get(finalI),finalI);
                            }
                        });
                    }
                }
            }
        }

        if(requestCode == GET_MEASUREMENTS_ALL){
            if(resultCode == BatchIAMActivity.RESULT_OK){
                    int[] roiArray = data.getIntArrayExtra("roi");
                    int scaleCalculatedDistance = data.getIntExtra("calculatedDistance", 1);
                    int scaleKnownDistance = data.getIntExtra("knownDistance",1);
                    String units = data.getStringExtra("units");
                    advancedImages.get(currentIndex).setScale(new Scale(scaleKnownDistance,units,scaleCalculatedDistance));
                    advancedImages.get(currentIndex).setRoi(new ROI(roiArray[0], roiArray[1], roiArray[2], roiArray[3]));
                    measurementsSet[currentIndex] = true;
                    Toast.makeText(this,"Measurements calculated for image "+(currentIndex+=1),Toast.LENGTH_SHORT).show();
            }
        }
        if(requestCode == GET_MEASUREMENTS_SCALE){
            if(resultCode == BatchIAMActivity.RESULT_OK){
                int scaleCalculatedDistance = data.getIntExtra("calculatedDistance", 1);
                int scaleKnownDistance = data.getIntExtra("knownDistance",1);
                String units = data.getStringExtra("units");
                advancedImages.get(currentIndex).setScale(new Scale(scaleKnownDistance,units,scaleCalculatedDistance));
                measurementsSet[currentIndex] = true;
                Toast.makeText(this,"Scale calculated for image "+(currentIndex+=1),Toast.LENGTH_SHORT).show();
            }
        }

        if(requestCode == GET_MEASUREMENTS_ROI){
            if(resultCode == BatchIAMActivity.RESULT_OK){
                int[] roiArray = data.getIntArrayExtra("roi");
                advancedImages.get(currentIndex).setRoi(new ROI(roiArray[0], roiArray[1], roiArray[2], roiArray[3]));
                measurementsSet[currentIndex] = true;
                Toast.makeText(this,"ROI calculated for image "+(currentIndex+=1),Toast.LENGTH_SHORT).show();
            }
        }
    }

    /*
      Validate name and insert request in database, pass AI ID to Analysis API
     */
    private void batchRequestInsertCall() {

         analysisPipeline = spinner.getSelectedItem().toString();

        if (analysisPipeline.equals("--- Select pipeline  ---")) {
            Toast.makeText(this, "You must select a valid pipeline", Toast.LENGTH_LONG);
            return;
        }
        analysisName = analysisNameEditT.getText().toString().trim();
        description = descriptionEditT.getText().toString().trim();
        int userId = UserManager.getInstance(this).getUserAccount().getLoginId();

        if (analysisName.isEmpty()) {
            analysisNameEditT.setError("You must enter an analysis name");
            analysisNameEditT.requestFocus();
            return;
        }
        if (description.isEmpty()) {
            descriptionEditT.setError("Description required");
            descriptionEditT.requestFocus();
            return;
        }

        Call<BatchRequestResponse> insertCall = Client.getClientInstance()
                .getRestAPI()
                .batchRequest(analysisName,pipelineVersionId, description, userId);
        insertCall.enqueue(new Callback<BatchRequestResponse>() {
            @Override
            public void onResponse(Call<BatchRequestResponse> call, Response<BatchRequestResponse> response) {

                if (!response.body().isError()) {
                    batchRequestId = response.body().getRequest_id();
                    if (pipelineProperties.isRoi() | pipelineProperties.isScale()) {
                        advancedBatchProcess();
                    } else {
                        batchProcess();
                    }
                } else {
                    Toast.makeText(BatchIAMActivity.this, "You have already used this name", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<BatchRequestResponse> call, Throwable t) {
                Toast.makeText(BatchIAMActivity.this, "Server is not responding",Toast.LENGTH_LONG).show();
            }
        });
    }

    /*
    Basic batch process not accessing measurements
     */
    private void batchProcess() {

        MultipartBody.Part[] multiPartBodyArray = new MultipartBody.Part[paths.size()];
        int userId = UserManager.getInstance(this).getUserAccount().getLoginId();
        pipelineProperties.setName(analysisPipeline);

        for(int i=0;i<paths.size();i++) {
            File file = new File(paths.get(i));
            RequestBody requestBody = RequestBody.create(MediaType.parse("*/*"), file);
            MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("img", file.getName(), requestBody);
            multiPartBodyArray[i] = fileToUpload;
        }

        analysisNameEditT.setText("");
        descriptionEditT.setText("");
        Toast.makeText(this,"Processing",Toast.LENGTH_LONG).show();

        Call<BatchResponse> call = ClientIAM.getClientInstance().getRestAPI()
                .batchProcess(
                multiPartBodyArray,analysisName,pipelineProperties,batchRequestId,userId);
        call.enqueue(new Callback<BatchResponse>() {
            @Override
            public void onResponse(Call<BatchResponse> call, Response<BatchResponse> response) {
                if(!response.body().isError()){
                    notifyOnUi(response);
                }
            }
            @Override
            public void onFailure(Call<BatchResponse> call, Throwable t) {
                Toast.makeText(BatchIAMActivity.this,t.getMessage(),Toast.LENGTH_LONG).show();
            }
        });
    }

    /*
      Advanced batch process accessing measurements
     */
    private void advancedBatchProcess() {

        for(int i=0;i<paths.size();i++){
            if(!measurementsSet[i]){
                Toast.makeText(this,"Please get all measurements",Toast.LENGTH_LONG).show();
                return;
            }
        }

        MultipartBody.Part[] multiPartBodyArray = new MultipartBody.Part[paths.size()];
        ROI[] roi = new ROI[paths.size()];

        Scale[] scale = new Scale[paths.size()];
        int userId = UserManager.getInstance(this).getUserAccount().getLoginId();
        pipelineProperties.setName(analysisPipeline);
        for(int i=0;i<paths.size();i++) {
            File file = new File(paths.get(i));
            RequestBody requestBody = RequestBody.create(MediaType.parse("*/*"), file);
            MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("img", file.getName(), requestBody);
            multiPartBodyArray[i] = fileToUpload;
            roi[i] = advancedImages.get(i).getRoi();
            scale[i] = advancedImages.get(i).getScale();
        }
        BatchROI roiArray = new BatchROI(roi);
        BatchScale scaleArrayObj = new BatchScale(scale);

        clearEditT();
        Toast.makeText(this,"Processing",Toast.LENGTH_LONG).show();

        Call<BatchResponse> call = ClientIAM.getClientInstance().getRestAPI()
                .advancedBatchProcess(
                        multiPartBodyArray,analysisName,pipelineProperties,batchRequestId,userId,roiArray,scaleArrayObj);
        call.enqueue(new Callback<BatchResponse>() {
            @Override
            public void onResponse(Call<BatchResponse> call, Response<BatchResponse> response) {
                if(!response.body().isError()){
                    notifyOnUi(response);
                }
            }

            @Override
            public void onFailure(Call<BatchResponse> call, Throwable t) {
                Toast.makeText(BatchIAMActivity.this,t.getMessage(),Toast.LENGTH_LONG).show();
            }
        });
    }

    public Intent prepareResultIntent(Response<BatchResponse> response){
        Intent intent = new Intent(BatchIAMActivity.this, BatchResultActivity.class);
        String[] imageNames = new String[response.body().getBatchResults().size()];
        String[] resultsPaths = new String[response.body().getBatchResults().size()];
        for (int i = 0; i <= response.body().getBatchResults().size() - 1; i++) {
            imageNames[i] = response.body().getBatchResults().get(i).getImagePath();
        }
        for (int i = 0; i <= response.body().getBatchResults().size() - 1; i++) {
            resultsPaths[i] = response.body().getBatchResults().get(i).getResultPath();
        }
        intent.putExtra("image_paths", imageNames);
        intent.putExtra("result_paths", resultsPaths);
        intent.putExtra("directory_name", response.body().getDirectory());
        intent.putExtra("analysis_name",analysisName);
        intent.putExtra("provides_results",pipelineProperties.isProducingResults());
        intent.putExtra("provides_image",pipelineProperties.isProducingImage());
        return intent;
    }

    /*
    API call fetching the pipelines which are batch ready
    Passing the response to a method to set the data in a spinner
     */
    private void fetchBatchReadyPipelines(User user) {
        Call<String> call = ClientScalar.getClientInstance().getRestAPI().batchPipeline(user.getLoginId());
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        String jsonresponse = response.body().toString();
                        displayPipelinesInSpinner(jsonresponse);
                    }
                }
            }
            @Override
            public void onFailure(Call<String> call, Throwable t) { }
        });
    }
     /*
     Parses JSON responses of the pipeline and pipeline version calls
     Creates objects of type "PipelineProperties" and adds them to arrays in class level scope
     Attaches data to spinner adapter
      */
    private void displayPipelinesInSpinner(String response) {

        try {
            JSONObject obj = new JSONObject(response);
            if (obj.optString("error").equals("false")) {
                if (obj.optString("message").equals("Versions")) {
                    pipelinePropertiesArrayList = new ArrayList<PipelineProperties>();
                    JSONArray dataArray = obj.getJSONArray("pipeline_versions");
                    Log.i("lenght", String.valueOf(dataArray.length()));
                    for (int i = dataArray.length()-1; i >=0; i--) {
                        JSONObject dataobj = dataArray.getJSONObject(i);
                        PipelineProperties pipelineVersion = new PipelineProperties();
                        pipelineVersion.setVersionNumber(dataobj.getString("Version"));
                        pipelineVersion.setRoi((dataobj.getInt("Requires_Roi") > 0)? true : false);
                        pipelineVersion.setScale((dataobj.getInt("Requires_Scale")>0) ? true : false);
                        pipelineVersion.setProducingImage((dataobj.getInt("Image")>0) ? true : false);
                        pipelineVersion.setProducingResults((dataobj.getInt("Result_Set")>0) ? true : false);
                        pipelineVersionId = dataobj.getInt("ID");
                        pipelinePropertiesArrayList.add(pipelineVersion);
                    }
                    ArrayList<String> versionNumbers = new ArrayList<>();
                    for(int i = 0; i< pipelinePropertiesArrayList.size(); i++){
                        versionNumbers.add(((pipelinePropertiesArrayList.get(i).getVersionNumber())));
                    }
                    ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(BatchIAMActivity.this, simple_spinner_item, versionNumbers);
                    spinnerArrayAdapter.setDropDownViewResource(simple_spinner_item); // The drop down view
                    versionSpinner.setAdapter(spinnerArrayAdapter);
                } else {
                    accessPipelinesArray = new ArrayList<Pipeline>();
                    JSONArray dataArray = obj.getJSONArray("pipeline");
                    for (int i = 0; i < dataArray.length(); i++) {
                        spinnerModel = new Pipeline();
                        JSONObject dataobj = dataArray.getJSONObject(i);
                        spinnerModel.setName(dataobj.getString("Name"));
                        spinnerModel.setID(dataobj.getInt("ID"));
                        spinnerModel.setVersions(dataobj.getInt("Versions"));
                        accessPipelinesArray.add(spinnerModel);
                    }
                    for (int i = 0; i < accessPipelinesArray.size(); i++) {
                        pipelineNames.add(accessPipelinesArray.get(i).getName().toString());
                    }
                    ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(BatchIAMActivity.this, simple_spinner_item, pipelineNames);
                    spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
                    spinner.setAdapter(spinnerArrayAdapter);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    /*
    Gets all versions and attributes for selected pipeline with API call using selected pipelineId
    passes response to method for applying response to version spinner
     */
    public void getPipelineVersions(int pipelineId) {
        Call<String> call = ClientScalar.getClientInstance().getRestAPI().pipelineVersions(pipelineId);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        String jsonresponse = response.body();
                        displayPipelinesInSpinner(jsonresponse);
                    }
                }
            }
            @Override
            public void onFailure(Call<String> call, Throwable t) { }});
    }

    private void notifyOnUi(Response<BatchResponse> response){
        Intent intent = prepareResultIntent(response);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this,NOTIFICATION_ID_STRING )
                .setSmallIcon(R.drawable.clipart4693381)
                .setContentTitle(analysisName)
                .setContentText("Your analysis has completed")
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true);
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(BatchIAMActivity.this);
        notificationManager.notify(0, builder.build());
    }

    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.channel_name);
            String description = getString(R.string.channel_description);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(NOTIFICATION_ID_STRING, name, importance);
            channel.setDescription(description);
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    private void measurementOperations(String path,int index) {

        if(pipelineProperties == null){
            return;
        }
        if(!pipelineProperties.isRoi() && !pipelineProperties.isScale()){
            return;
        }

        Intent intent = new Intent(this,SetBatchMeasurementsActivity1.class);
        intent.putExtra("image_path",path);
        /*
        Current image pressed, used to populate the specific object with the returned data.
         */
        currentIndex = index;
        if(pipelineProperties.isRoi()& pipelineProperties.isScale()){
            intent.putExtra("requestCode", GET_MEASUREMENTS_ALL);
            startActivityForResult(intent, GET_MEASUREMENTS_ALL);
        }else{
            if(pipelineProperties.isRoi()){
                intent.putExtra("requestCode", GET_MEASUREMENTS_ROI);
                startActivityForResult(intent, GET_MEASUREMENTS_ROI);
            }else {
                startActivityForResult(intent, GET_MEASUREMENTS_SCALE);
                intent.putExtra("requestCode", GET_MEASUREMENTS_SCALE);
            }
        }
    }

    private void clearEditT(){
        analysisNameEditT.setText("");
        descriptionEditT.setText("");
    }
}
