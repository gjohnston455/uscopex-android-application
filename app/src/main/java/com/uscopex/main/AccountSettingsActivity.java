package com.uscopex.main;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.gareth.R;

import com.uscopex.api.Client;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import com.uscopex.storage.UserManager;

public class AccountSettingsActivity extends AppCompatActivity implements View.OnClickListener {

    private int userId;

    EditText newEmail,currentPasswordEmail,currentPassword, newPassword, newPasswordConfirm;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        userId = UserManager.getInstance(this).getUserAccount().getLoginId();
        setContentView(R.layout.account_settings_layout);
        newEmail = findViewById(R.id.editTextEmail);
        currentPasswordEmail = findViewById(R.id.editTextTextEmailPasswordConfirm);
        newPasswordConfirm = findViewById(R.id.editTextTextPasswordConfirm);
        currentPassword = findViewById(R.id.editTextCurrentPassw);
        newPassword = findViewById(R.id.editTextNewPassword);

        findViewById(R.id.emailButton).setOnClickListener(this);
        findViewById(R.id.passwordButton).setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {

        switch (view.getId()){
            case R.id.emailButton:
                changeEmail();
            break;
            case R.id.passwordButton:
              changePassword();
            default:
                break;
        }
    }

    private void changePassword() {

        String currentPassw = currentPassword.getText().toString().trim();
        String newPassw = newPassword.getText().toString().trim();
        String confirmNewPass = newPasswordConfirm.getText().toString().trim();

        if(currentPassw.isEmpty()){
            currentPassword.setError("Enter your current password");
            currentPassword.requestFocus();
            return;
        }

        if(newPassw.isEmpty()){
            newPassword.setError("You must enter your new password");
            newPassword.requestFocus();
            return;
        }

        if(confirmNewPass.isEmpty()){
            newPasswordConfirm.setError("You must enter confirm your new password");
            newPasswordConfirm.requestFocus();
            return;
        }

        if(!newPassw.equals(confirmNewPass)){
            newPasswordConfirm.setError("Your passwords do not match");
            newPasswordConfirm.requestFocus();
            return;
        }
        changePasswordCall(currentPassw,newPassw);
    }

    private void changePasswordCall(String currentPass, String newPass) {

        Call<ResponseBody> call = Client.getClientInstance().getRestAPI().changePassword(currentPass,newPass,userId);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.isSuccessful()){
                    Toast.makeText(AccountSettingsActivity.this,"Password changed",Toast.LENGTH_LONG).show();
                   currentPassword.setText("");
                   newPassword.setText("");
                   newPasswordConfirm.setText("");
                }else{
                    Toast.makeText(AccountSettingsActivity.this,"Incorrect password, try again",Toast.LENGTH_LONG).show();
                }
            }
            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) { }
        });
    }

    private void changeEmail() {

        String emailText = newEmail.getText().toString().trim();
        String password = currentPasswordEmail.getText().toString().trim();

        if(emailText.isEmpty()){
            newEmail.setError("You must enter a valid email");
            newEmail.requestFocus();
            return;
        }
        if(password.isEmpty()){
            currentPasswordEmail.setError("You must enter your password");
            currentPasswordEmail.requestFocus();
            return;
        }
        changeEmailApiCall(emailText,userId,password);
    }

    private void changeEmailApiCall(String email, int userId, String password) {

        Call<ResponseBody> call = Client.getClientInstance().getRestAPI().changeEmail(email,userId,password);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.isSuccessful()){
                    Toast.makeText(AccountSettingsActivity.this,"Email changed",Toast.LENGTH_LONG).show();
                    newEmail.setText(" ");
                    currentPassword.setText("");
                }else{
                    Toast.makeText(AccountSettingsActivity.this,"Password is not correct",Toast.LENGTH_LONG).show();
                }
            }
            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) { }
        });
    }
}
